using System;
using System.ServiceProcess;

namespace CacheService
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static int Main(string[] args)
        {
            bool serviceMode = !((args.Length > 0) && (args[0].ToLower() == "noservice"));

            var servicesToRun = new ServiceBase[] {new CacheService(!serviceMode)};

            if (serviceMode)
            {
                ServiceBase.Run(servicesToRun);
            }
            else
            {
                try
                {
                    foreach (CacheService serv in servicesToRun)
                        serv.DirectStart(args);

                    Console.WriteLine("Service started, hit enter to finish");
                    Console.Read();
                    foreach (CacheService serv in servicesToRun)
                        serv.DirectStop();
                    Console.WriteLine("Services stopped");
                    return 0;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Fatal exception {0}", ex.Message);
                    return -1;
                }
            }

            return 0;
        }
    }
}