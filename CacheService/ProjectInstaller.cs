using System.ComponentModel;
using System.Configuration.Install;

namespace CacheService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}