using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using Cachealot.Server;
using Channel;

namespace CacheService
{
    public partial class CacheService : ServiceBase
    {
        private Server _cacheServer;
        private bool _consoleMode;

        private TcpServerChannel _listener;

        public CacheService() : this(false)
        {
        }

        public CacheService(bool consoleMode)
        {
            _consoleMode = consoleMode;
            InitializeComponent();
        }

        public void DirectStart(string[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            ServerConfig config;

            try
            {
                string assemblyLocation = Assembly.GetAssembly(typeof(CacheService)).Location;
                string directoryName = Path.GetDirectoryName(assemblyLocation);
                string fullCfgPath = Path.Combine(directoryName, "cacheserver.xml");
                config = ServerConfig.LoadFromFile(fullCfgPath);
            }
            catch (Exception ex)
            {
                string message = string.Format("Can not read configuration file: {0}", ex.Message);

                if (!_consoleMode)
                    eventLog.WriteEntry(message, EventLogEntryType.Error);
                else
                {
                    Console.WriteLine(message);
                }

                throw;
            }


            _cacheServer = new Server(config);
            
            _listener = new TcpServerChannel();
            _cacheServer.Channel = _listener;
            _listener.Init(config.TcpPort);
            _listener.Start();

            _cacheServer.Start();
            //_cacheServer.Stop();

            string msg = string.Format("CacheALot server is started on port {0}", config.TcpPort);

            if (!_consoleMode)
                eventLog.WriteEntry(msg, EventLogEntryType.Information);
            else
            {
                Console.WriteLine(msg);
            }
        }

        public void DirectStop()
        {
            OnStop();
        }

        protected override void OnStop()
        {
            _listener.Stop();
            _cacheServer.Stop();
        }
    }
}