using System;
using System.Linq;
using System.Linq.Expressions;
using CacheClient.Core;
using CacheClient.Interface;
using CacheClient.Linq;
using Channel;
using Remotion.Linq;
using Remotion.Linq.Parsing.Structure;

namespace DataSource.Linq
{
    public class DataSource<T> : QueryableBase<T>
    {
        private readonly ICacheClient _client;


        public DataSource(ClientConfig config) : base(QueryParser.CreateDefault(), new QueryExecutor(null))
        {
            if (config.Servers == null || config.Servers.Count == 0)
            {
                var channel = new InProcessChannel();
                _client = new CacheClient.Core.CacheClient {Channel = channel};
            }
            else if (config.Servers.Count == 1)
            {
                var serverCfg = config.Servers[0];

                var channel = new TcpClientChannel(new TcpClientPool(4, 1, serverCfg.Host, serverCfg.Port));

                _client = new CacheClient.Core.CacheClient {Channel = channel};
            }
            else // multiple servers
            {
                var aggregator = new Aggregator();

                foreach (var serverConfig in config.Servers)
                {
                    var channel = new TcpClientChannel(new TcpClientPool(4, 1, serverConfig.Host, serverConfig.Port));

                    var client = new CacheClient.Core.CacheClient {Channel = channel};
                    aggregator.CacheClients.Add(client);
                }


                _client = aggregator;
            }


            // register types from client configuration
            foreach (var description in config.TypeDescriptions)
            {
                Type type = Type.GetType(description.Value.FullTypeName);
                _client.RegisterType(type, description.Value);
            }
        }

        public DataSource(CacheClient.Core.CacheClient client) : base(QueryParser.CreateDefault(),
            new QueryExecutor(client))
        {
            _client = client;
        }

        public DataSource(IQueryParser queryParser, IQueryExecutor executor)
            : base(new DefaultQueryProvider(typeof(DataSource<>), queryParser, executor))
        {
        }

        public DataSource(IQueryProvider provider, Expression expression)
            : base(provider, expression)
        {
        }


        /// <summary>
        /// Update or insert an object
        /// </summary>
        /// <param name="item"></param>
        public void Put(T item)
        {
            _client.Put(item);
        }


        /// <summary>
        /// Get one item by primary key. Returns null if none
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        public T this[object primaryKey] => _client.GetOne<T>(primaryKey);
    }
}