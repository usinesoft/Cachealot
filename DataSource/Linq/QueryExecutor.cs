using System;
using System.Collections.Generic;
using System.Linq;
using DataSource.Linq;
using Remotion.Linq;

namespace CacheClient.Linq
{
    public class QueryExecutor : IQueryExecutor
    {
        private readonly Core.CacheClient _client;

        public QueryExecutor(Core.CacheClient client)
        {
            _client = client;
        }

        // Executes a query with a scalar result, i.e. a query that ends with a result operator such as Count, Sum, or Average.
        public T ExecuteScalar<T>(QueryModel queryModel)
        {
            var visitor = new QueryVisitor<T>();

            visitor.VisitQueryModel(queryModel);

            var expression = visitor.RootExpression;

            System.Console.WriteLine(expression);

            if (expression.CountOnly)
            {
                return (T) (object) 0;
            }

            throw new NotSupportedException("Only Count scalar method is implemented");
        }

        // Executes a query with a single result object, i.e. a query that ends with a result operator such as First, Last, Single, Min, or Max.
        public T ExecuteSingle<T>(QueryModel queryModel, bool returnDefaultWhenEmpty)
        {
            return returnDefaultWhenEmpty
                ? ExecuteCollection<T>(queryModel).SingleOrDefault()
                : ExecuteCollection<T>(queryModel).Single();
        }

        // Executes a query with a collection result.
        public IEnumerable<T> ExecuteCollection<T>(QueryModel queryModel)
        {
            var visitor = new QueryVisitor<T>();

            visitor.VisitQueryModel(queryModel);

            var expression = visitor.RootExpression;

#if DEBUG
            System.Console.WriteLine(expression);
#endif

            return _client.GetMany<T>(visitor.RootExpression);
        }
    }
}