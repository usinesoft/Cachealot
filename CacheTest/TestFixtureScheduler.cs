using System;
using System.Collections.Generic;
using System.Data;
using Cachealot.Server;
using NUnit.Framework;

namespace CacheTest
{
    [TestFixture]
    public class TestFixtureScheduler
    {
        [SetUp]
        public void SetUp()
        {
            _sharedResource = new List<int>();
            _scheduler = new LockFreeScheduler<object>(4);
        }

        private List<int> _sharedResource;
        private LockFreeScheduler<object> _scheduler;

        [Test]
        public void ExecuteMixedWorkItems()
        {
            _scheduler.Start();

            WorkItem<object>.ResetTaskCount();

            const int TASKS = 1000;

            for (int i = 0; i < TASKS; i++)
            {
                //one write-access task for nine read-only accesses
                //the write-access task is clearing thei tems in the list, filling it with random data and sorting
                //Fist task must a write-access one in order to initialize the data
                if (i % 10 == 0)
                {
                    WorkItem<object> writeWorkitem = new WorkItem<object>(true, null,
                        delegate
                        {
                            Random rand =
                                new Random(Environment.TickCount);
                            _sharedResource.Clear();
                            for (int j = 0; j < 100; j++)
                            {
                                _sharedResource.Add(rand.Next());
                            }

                            _sharedResource.Sort();
                        }
                    );

                    _scheduler.ProcessTask(writeWorkitem);
                }
                else //read-only access (check that data is allways sorted)
                {
                    WorkItem<object> readOnlyWorkitem = new WorkItem<object>(false, null,
                        delegate
                        {
                            for (int j = 0; j < 100 - 1; j++)
                            {
                                if (_sharedResource[j] > _sharedResource[j + 1])
                                {
                                    string msg =
                                        string.Format(
                                            "Collection is not sorted, probably race condition detected, data[{0}] = {1} data [{2}] = {3}",
                                            j, j + 1, _sharedResource[j], _sharedResource[j + 1]);
                                    throw new DataException(msg);
                                }
                            }
                        }
                    );

                    _scheduler.ProcessTask(readOnlyWorkitem);
                }
            }


            _scheduler.Stop();

            Assert.AreEqual(TASKS, WorkItem<object>.ExecutedTaskCount);
        }

        [Test]
        public void ExecuteWriteWorkItems()
        {
            _scheduler.Start();

            WorkItem<object>.ResetTaskCount();

            const int TASKS = 1000;
            const int DATA_SIZE = 100;

            for (int i = 0; i < TASKS; i++)
            {
                //one write-access task for nine read-only accesses
                //the write-access task is clearing thei tems in the list, filling it with random data and sorting
                //Fist task must a write-access one in order to initialize the data
                if (i % 10 == 0)
                {
                    WorkItem<object> writeWorkitem = new WorkItem<object>(true, null,
                        delegate
                        {
                            Random rand =
                                new Random(Environment.TickCount);
                            _sharedResource.Clear();
                            for (int j = 0; j < DATA_SIZE; j++)
                            {
                                _sharedResource.Add(rand.Next());
                            }

                            _sharedResource.Sort();
                        }
                    );

                    _scheduler.ProcessTask(writeWorkitem);
                }
                else //read-only access declared as write acces(check that data is allways sorted)
                {
                    WorkItem<object> readOnlyWorkitem = new WorkItem<object>(true, null,
                        delegate
                        {
                            if (_sharedResource.Count != DATA_SIZE)
                            {
                                string msg =
                                    string.Format(
                                        "Collection is not completely filled, probably race condition detected (only {0} items)",
                                        _sharedResource.Count);
                                throw new DataException(msg);
                            }

                            for (int j = 0; j < DATA_SIZE - 1; j++)
                            {
                                if (_sharedResource[j] > _sharedResource[j + 1])
                                {
                                    string msg =
                                        string.Format(
                                            "Collection is not sorted, probably race condition detected, data[{0}] = {1} data [{2}] = {3}",
                                            j, j + 1, _sharedResource[j], _sharedResource[j + 1]);
                                    throw new DataException(msg);
                                }
                            }
                        }
                    );

                    _scheduler.ProcessTask(readOnlyWorkitem);
                }
            }


            _scheduler.Stop();

            Assert.AreEqual(TASKS, WorkItem<object>.ExecutedTaskCount);
        }
    }
}