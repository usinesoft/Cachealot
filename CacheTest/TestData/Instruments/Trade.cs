﻿using System;
using CacheClient.Interface;

namespace CacheTest.TestData.Instruments
{

    
    public enum AssetClass
    {
        Equity,
        Ird,
        Creder,
        Fx
    }

    public class EquityOption : IProduct
    {
        public enum OptionType
        {
            Call,
            Put
        };

        public enum ExerciseType
        {
            American,
            European
        };

        public enum SettlementType
        {
            Cash,
            Physical
        };


        public string Name => "EquityOption";
        public AssetClass AssetClass => AssetClass.Equity;

        public string Underlying { get; set; }

        public decimal Strike { get; set; }
        public decimal UnitPrice { get; set; }

        public OptionType Type { get; set; }

        public ExerciseType Exercise { get; set; }

        public SettlementType Settlement { get; set; }

        public DateTime? MaturityDate { get; set; }

        public decimal Quantity { get; set; }

        public decimal Notional => Quantity * UnitPrice;
    }

    public interface IProduct
    {
        string Name { get; }

        AssetClass AssetClass { get; }
        
        /// <summary>
        /// Not all assets have maturity date
        /// </summary>
        DateTime? MaturityDate { get; }

        decimal Quantity { get; }

        decimal UnitPrice { get; }

        decimal Notional { get; }



    }

    public class Trade
    {
        [PrimaryKey(KeyDataType.IntKey)]
        public int Id { get; set; }

        [Index(KeyDataType.IntKey)]
        public int Version { get; set; }

        [Index(KeyDataType.StringKey)]
        public string ContractId { get; set; }

        [Index(KeyDataType.IntKey)]
        public bool IsDestroyed { get; set; }

        [Index(KeyDataType.IntKey)]
        public bool IsLastVersion { get; set; }

        [Index(KeyDataType.IntKey, true)]
        public DateTime TradeDate { get; set; }

        [Index(KeyDataType.IntKey, true)]
        public DateTime ValueDate { get; set; }

        [Index(KeyDataType.IntKey, true)]
        public DateTime Timestamp { get; set; }

        [Index(KeyDataType.StringKey)]
        public string Portfolio { get; set; }

        [Index(KeyDataType.StringKey)]
        public string Counterparty { get; set; }

        public IProduct Product { get; set; }

    }
}
