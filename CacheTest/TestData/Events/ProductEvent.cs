﻿using System;
using CacheClient.Interface;
// ReSharper disable NonReadonlyMemberInGetHashCode

namespace CacheTest.TestData.Events
{
    public abstract class ProductEvent
    {
        [PrimaryKey(KeyDataType.StringKey)]
        public int EventId { get; set; }

        [Index(KeyDataType.StringKey)]
        public abstract string EventType { get;  }

        public string Comment { get; set; }

        [Index(KeyDataType.StringKey)]
        public string DealId { get; set; }


        [Index(KeyDataType.IntKey, true)]
        public DateTime EventDate { get; set; }

        [Index(KeyDataType.IntKey, true)]
        public DateTime ValueDate{ get; set; }


        protected bool Equals(ProductEvent other)
        {
            return EventId == other.EventId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ProductEvent) obj);
        }

        public override int GetHashCode()
        {
            return EventId;
        }

        public static bool operator ==(ProductEvent left, ProductEvent right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ProductEvent left, ProductEvent right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return $"{nameof(EventId)}: {EventId}, {nameof(EventType)}: {EventType}";
        }
    }

    public class FixingEvent:ProductEvent
    {
        public string Underlying { get; set; }

        public Decimal Value { get; set; }

        public DateTime FixingDate { get; set; }

        public override string EventType => "FIXING";


        public FixingEvent(int id, string underlying, decimal value, string dealId)
        {
            EventId = id;
            Underlying = underlying;
            Value = value;

            DealId = dealId;

            FixingDate = DateTime.Today;
            EventDate = DateTime.Today;
            ValueDate = DateTime.Today;
        }
    }




    public abstract class NegotiatedProductEvent : ProductEvent
    {

        public abstract bool NeedsConfirmation { get; }

    }

    public class Increase : NegotiatedProductEvent
    {
        public Decimal Delta { get; set; }

        public override bool NeedsConfirmation => true;

        public override string EventType => "INCREASE";

        public Increase(int id, decimal delta, string dealId)
        {

            EventId = id;
            Delta = delta;

            DealId = dealId;

            EventDate = DateTime.Today;
            ValueDate = DateTime.Today;
            
        }
    }

    public class Create : NegotiatedProductEvent
    {
        
        public override bool NeedsConfirmation => true;

        public override string EventType => "CREATE";

        public Create(int id, string dealId)
        {

            EventId = id;
            
            DealId = dealId;

            EventDate = DateTime.Today;
            ValueDate = DateTime.Today;

        }
    }
}
