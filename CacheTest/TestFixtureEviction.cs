#region

using System;
using System.Collections.Generic;
using System.Linq;
using Cachealot.Server;
using CacheClient.Interface;
using CacheTest.TestData;
using Channel;
using NUnit.Framework;
using ServerConfig = Cachealot.Server.ServerConfig;

#endregion

namespace CacheTest
{
    [TestFixture]
    public class TestFixtureEviction
    {
        [SetUp]
        public void Init()
        {
            _client = new CacheClient.Core.CacheClient();
            var channel = new InProcessChannel();
            _client.Channel = channel;

            var cfg = new ServerConfig();
            var cfgDatastore = new ServerDatastoreConfig
            {
                Eviction =
                    new EvictionPolicyConfig(
                        EvictionType.LessRecentlyUsed, 100, 10),
                FullTypeName = typeof(CacheableTypeOk).FullName
            };
            //activate eviction when more than 100 items in cache (evict 10 items)
            cfg.ConfigByType.Add(cfgDatastore.FullTypeName, cfgDatastore);
            _server = new Server(cfg) {Channel = channel};
            _server.Start();

            _client.RegisterTypeIfNeeded(typeof(CacheableTypeOk));
        }

        [TearDown]
        public void TearDown()
        {
            _server.Stop();
        }

        private CacheClient.Core.CacheClient _client;

        private Server _server;

        [Test]
        public void EvictionLimitIsReached()
        {
            var desc = _client.GetServerDescription();
            Assert.IsNotNull(desc);
            Assert.AreEqual(desc.DataStoreInfoByFullName.Count, 1);
            var info = desc.DataStoreInfoByFullName[typeof(CacheableTypeOk).FullName];
            Assert.IsNotNull(info);
            Assert.AreEqual(info.EvictionPolicy, EvictionType.LessRecentlyUsed);

            //add one item
            var item1 = new CacheableTypeOk(1, 1001, "aaa", new DateTime(2010, 10, 10), 1500);
            _client.Put(item1);

            //reload the item by primary key
            var item1Reloaded = _client.GetOne<CacheableTypeOk>(1);
            Assert.AreEqual(item1, item1Reloaded);

            //add 100 items; eviction should be triggered(10 items should be removed)
            for (int i = 0; i < 100; i++)
            {
                var item = new CacheableTypeOk(i + 2, i + 1002, "aaa", new DateTime(2010, 10, 10), 1500);
                _client.Put(item);
            }

            //reload all items
            IList<CacheableTypeOk> itemsInAaa = _client.GetMany<CacheableTypeOk>("IndexKeyFolder == aaa").ToList();
            Assert.AreEqual(itemsInAaa.Count, 90); //(100 - 10)(capacity-evictionCount)

            var itemsAsList = new List<CacheableTypeOk>(itemsInAaa);
            itemsAsList.Sort((x, y) => x.PrimaryKey.CompareTo(y.PrimaryKey));

            //check that the first 11 items were evicted
            Assert.IsTrue(itemsAsList[0].PrimaryKey == 12);

            //update the first item. This should prevent it from being evicted
            _client.Put(new CacheableTypeOk(11, 1001, "aaa", new DateTime(2010, 10, 10), 1500));

            for (int i = 0; i < 10; i++)
            {
                var item = new CacheableTypeOk(i + 102, i + 1102, "aaa", new DateTime(2010, 10, 10), 1500);
                _client.Put(item);
            }

            itemsInAaa = _client.GetMany<CacheableTypeOk>("IndexKeyFolder == aaa").ToList();
            Assert.AreEqual(itemsInAaa.Count, 90); //(100 - 10)

            //check that the eviction operation is registered in the server log
            var log = _client.GetServerLog(10);
            // log of the GET happens on another thread, after the response has been sent, so it may not have been done yet
            // on the other hand, the write-access for EVICTION ends after the log, so we could proceed with
            // the GET only if the EVICTION has been completed, including the log
            Assert.AreEqual(
                log.Entries[0].RequestType == "GET" ? log.Entries[1].RequestType : log.Entries[0].RequestType,
                "EVICTION");

            itemsAsList = new List<CacheableTypeOk>(itemsInAaa);
            itemsAsList.Sort((x, y) => x.PrimaryKey.CompareTo(y.PrimaryKey));

            //check that the item having 11 as primary key was not evicted
            Assert.IsTrue(itemsAsList[0].PrimaryKey == 11);
            Assert.IsTrue(itemsAsList[1].PrimaryKey == 23);
        }

        [Test]
        public void EvictionLimitIsReachedUsingPutMany()
        {
            var desc = _client.GetServerDescription();
            Assert.IsNotNull(desc);
            Assert.AreEqual(desc.DataStoreInfoByFullName.Count, 1);
            var info = desc.DataStoreInfoByFullName[typeof(CacheableTypeOk).FullName];
            Assert.IsNotNull(info);
            Assert.AreEqual(info.EvictionPolicy, EvictionType.LessRecentlyUsed);

            //add one item
            var item1 = new CacheableTypeOk(1, 1001, "aaa", new DateTime(2010, 10, 10), 1500);
            _client.Put(item1);

            //reload the item by primary key
            var item1Reloaded = _client.GetOne<CacheableTypeOk>(1);
            Assert.AreEqual(item1, item1Reloaded);

            //add 100 items; eviction should be triggered(10 items should be removed)
            var itemsToPut = new List<CacheableTypeOk>();
            for (int i = 0; i < 100; i++)
            {
                var item = new CacheableTypeOk(i + 2, i + 1002, "aaa", new DateTime(2010, 10, 10), 1500);
                itemsToPut.Add(item);
            }

            _client.PutMany(itemsToPut, false);

            //reload all items
            IList<CacheableTypeOk> itemsInAaa = _client.GetMany<CacheableTypeOk>("IndexKeyFolder == aaa").ToList();
            Assert.AreEqual(itemsInAaa.Count, 90); //(100 - 10)(capacity-evictionCount)
        }

        [Test]
        public void ExcludedItemsAreNotEvicted()
        {
            var itemNotToBeEvicted = new CacheableTypeOk(0, 1000, "aaa", new DateTime(2010, 10, 10), 1500);
            _client.Put(itemNotToBeEvicted, true);

            for (int i = 0; i < 101; i++)
            {
                var item = new CacheableTypeOk(i + 1, i + 1001, "aaa", new DateTime(2010, 10, 10), 1500);
                _client.Put(item);
            }

            IList<CacheableTypeOk> itemsInAaa = _client.GetMany<CacheableTypeOk>("IndexKeyFolder == aaa").ToList();
            var itemsAsList = new List<CacheableTypeOk>(itemsInAaa);
            itemsAsList.Sort((x, y) => x.PrimaryKey.CompareTo(y.PrimaryKey));

            //check that the first item was not evicted
            Assert.AreEqual(itemsAsList.Count, 91);
            Assert.IsTrue(itemsAsList[0].PrimaryKey == 0);
        }
    }
}