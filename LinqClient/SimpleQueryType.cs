﻿namespace Cachealot.Linq
{
    public enum SimpleQueryType
    {
        Single,
        Conjunction,
        Disjunction
    }
}