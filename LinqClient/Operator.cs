﻿namespace Cachealot.Linq
{
    /// <summary>
    ///     Basic operators that can be used in <see cref="AbstractDataContext{T}" />
    /// </summary>
    public enum Operator
    {
        Lt,
        Le,
        Eq,
        Ge,
        Gt,
        Btw
    }
}