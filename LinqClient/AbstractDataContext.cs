﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqExtender;
using LinqExtender.Ast;

namespace Cachealot.Linq
{
    /// <summary>
    ///     A data context that does not have a provider. It transforms the "where" clause into a <see cref="SimpleQuery" />
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AbstractDataContext<T> : IQueryContext<T>
    {
        /// <summary>
        ///     Client-provided code that effectively gets data from an external provider
        /// </summary>
        public Func<SimpleQuery, IEnumerable<T>> ResultProducer { get; set; }


        private static SimpleQueryType ConvertOperator(LogicalExpression expr)
        {
            var qt = SimpleQueryType.Single;

            switch (expr.Operator)
            {
                case LogicalOperator.And:
                    qt = SimpleQueryType.Conjunction;
                    break;
                case LogicalOperator.Or:
                    qt = SimpleQueryType.Disjunction;
                    break;
            }

            return qt;
        }


        private static SimpleQueryType ProcessIfUniform(Expression root, IList<AtomicExpression> atoms,
                                                        SimpleQueryType type)
        {
            if (root.CodeType == CodeType.BinaryExpression)
            {
                var binary = (BinaryExpression) root;

                if ((binary.Left.CodeType == CodeType.MemberExpression) &&
                    (binary.Right.CodeType == CodeType.LiteralExpression))
                {
                    var member = (MemberExpression) binary.Left;
                    var literal = (LiteralExpression) binary.Right;

                    var op = Operator.Eq;
                    switch (binary.Operator)
                    {
                        case BinaryOperator.Equal:
                            op = Operator.Eq;
                            break;
                        case BinaryOperator.LessThan:
                            op = Operator.Lt;
                            break;
                        case BinaryOperator.LessThanEqual:
                            op = Operator.Le;
                            break;
                        case BinaryOperator.GreaterThan:
                            op = Operator.Gt;
                            break;
                        case BinaryOperator.GreaterThanEqual:
                            op = Operator.Ge;
                            break;
                    }


                    atoms.Add(new AtomicExpression
                                  {
                                      Property = member.Member.MemberInfo,
                                      Value = literal.Value,
                                      Operator = op
                                  });

                    return type;
                }
            }
            else if (root.CodeType == CodeType.LogicalExpression)
            {
                var logical = (LogicalExpression) root;
                var qt = ConvertOperator(logical);
                if (type == qt || type == SimpleQueryType.Single)
                {
                    ProcessIfUniform(logical.Left, atoms, qt);

                    return ProcessIfUniform(logical.Right, atoms, qt);
                }

                throw new NotSupportedException("Nonuniform logical expression: " + root);
            }
            else
            {
                throw new NotSupportedException("Expression too complex: " + root);
            }

            return type;
        }

        public static SimpleQuery AsSimpleQuery(Expression expression)
        {
            if (expression.CodeType == CodeType.BlockExpression)
            {
                var block = (BlockExpression) expression;
                if (block.Expressions.Count == 2)
                {
                    var where = (LambdaExpression) block.Expressions[1];

                    var sq = new SimpleQuery();
                    sq.QueryType = ProcessIfUniform(where.Body, sq.Elements, SimpleQueryType.Single);

                    return sq;
                }
            }

            throw new NotSupportedException("Unknown expression type: " + expression);
        }


        public Exception LastException { get; set; }

        public IEnumerable<T> Execute(Expression expression)
        {
            LastQuery = AsSimpleQuery(expression);

            // optimize the query. Example transform "a>1 and a<2" in "a BTW 1 2" which is much faster to evaluate 
            PreprocessSimpleQuery(LastQuery);

            LastException = null;

            if (ResultProducer != null)
            {
                try
                {
                    return ResultProducer(LastQuery);
                }
                catch (Exception e)
                {
                    LastException = e;
                }
            }

            return new List<T>();
        }


       
        /// <summary>
        /// Apply client size optimizations on the input query
        /// </summary>
        /// <param name="query"></param>
        private void PreprocessSimpleQuery(SimpleQuery query)
        {
            var queryByKey = (from e in query.Elements group e by e.Property into grp select new {grp.Key, Values = grp.ToList()}).ToList();

            query.Elements.Clear();

            foreach (var byKey in queryByKey)
            {
                bool wasConverted = false;

                // convert "a <= end and a >= start" into "a between start, end" 
                if (byKey.Values.Count == 2 )
                {
                    var firstOperator = byKey.Values[0].Operator;
                    var secondOperator = byKey.Values[1].Operator;

                    if (firstOperator == Operator.Le  && secondOperator == Operator.Ge)// the end value is the first
                    {
                        query.Elements.Add(new AtomicExpression
                                               {
                                                   Operator = Operator.Btw, 
                                                   Value = byKey.Values[1].Value, 
                                                   Value2 = byKey.Values[0].Value,
                                                   Property = byKey.Values[0].Property
                                               });

                        wasConverted = true;
                    }
                    else if(firstOperator == Operator.Ge  && secondOperator == Operator.Le)// the start value is first
                    {
                        query.Elements.Add(new AtomicExpression
                                               {
                                                   Operator = Operator.Btw,
                                                   Value = byKey.Values[0].Value,
                                                   Value2 = byKey.Values[1].Value,
                                                   Property = byKey.Values[0].Property
                                               });

                        wasConverted = true;
                    }
                }


                if (!wasConverted)
                {
                    foreach (var expression in byKey.Values)
                    {
                        query.Elements.Add(expression);
                    }
                }
            }
            
        }

        public SimpleQuery LastQuery { get; private set; }
    }
}