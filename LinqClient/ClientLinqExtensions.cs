﻿#region

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CacheClient.Interface;
using CacheClient.Queries;
using LinqExtender;

#endregion

namespace Cachealot.Linq
{
    /// <summary>
    ///     Linq adapter implemented ad extension methods for the <see cref="ICacheClient" /> interface
    /// </summary>
    public static class ClientLinqExtensions
    {
        private static QueryOperator ConvertOperator(AtomicExpression expr)
        {
            QueryOperator qt;

            switch (expr.Operator)
            {
                case Operator.Gt:
                    qt = QueryOperator.Gt;
                    break;
                case Operator.Ge:
                    qt = QueryOperator.Ge;
                    break;
                case Operator.Lt:
                    qt = QueryOperator.Lt;
                    break;
                case Operator.Le:
                    qt = QueryOperator.Le;
                    break;

                case Operator.Eq:
                    qt = QueryOperator.Eq;
                    break;
                case Operator.Btw:
                    qt = QueryOperator.Btw;
                    break;

                default:
                    throw new NotSupportedException("Can not use this operator with LINQ " + expr.Operator);
            }

            return qt;
        }

        public static OrQuery BuildCacheQuery<TItemType>(SimpleQuery query)
        {
            // a cache query is allways a disjunction of conjunctions of atomic queries
            var builder = new QueryBuilder(typeof (TItemType));

            var orQuery = builder.MakeOrQuery();

            if (query.QueryType == SimpleQueryType.Disjunction || query.QueryType == SimpleQueryType.Single)
            {
                foreach (var atomicExpression in query.Elements)
                {
                    var atomicQuery = atomicExpression.Operator ==Operator.Btw? 
                        builder.MakeAtomicQuery(atomicExpression.Property.Name, atomicExpression.Value, atomicExpression.Value2):
                        builder.MakeAtomicQuery(atomicExpression.Property.Name, ConvertOperator(atomicExpression), atomicExpression.Value)
                        ;

                    var andQuery = builder.MakeAndQuery(atomicQuery);

                    orQuery.Elements.Add(andQuery);
                }
            }
            else // it's a conjunction
            {
                var andQuery = builder.MakeAndQuery();
                foreach (var atomicExpression in query.Elements)
                {
                    var atomicQuery = atomicExpression.Operator == Operator.Btw ?
                       builder.MakeAtomicQuery(atomicExpression.Property.Name, atomicExpression.Value, atomicExpression.Value2) :
                       builder.MakeAtomicQuery(atomicExpression.Property.Name, ConvertOperator(atomicExpression), atomicExpression.Value)
                       ;

                    andQuery.Elements.Add(atomicQuery);
                }

                orQuery.Elements.Add(andQuery);
            }

            return orQuery;
        }

        public static IEnumerable<TItemType> GetMany<TItemType>(this ICacheClient cache,
                                                                Expression<Func<TItemType, bool>> condition,
                                                                bool onlyIfFullyLoaded = false) where TItemType : class
        {
            try
            {
                var ctx = new AbstractDataContext<TItemType>
                              {
                                  ResultProducer =
                                      query =>
                                      cache.GetMany<TItemType>(
                                          BuildCacheQuery<TItemType>(query),
                                          onlyIfFullyLoaded)
                              };

                // the exception was stored as we do not want it to be intercepted by the provider
                if (ctx.LastException != null)
                    throw ctx.LastException;

                return ctx.Where(condition);
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                    var cacheException = e.InnerException as CacheException;
                    if (cacheException != null)
                        throw cacheException;
                }
            }


            return new List<TItemType>(); // empty result
        }
    }
}
