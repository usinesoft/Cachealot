﻿using System.Reflection;
using System.Text;

namespace Cachealot.Linq
{
    /// <summary>
    ///     A simple predicate of the form property + operator + literal value
    /// </summary>
    public class AtomicExpression
    {
        public MemberInfo Property { get; set; }

        public Operator Operator { get; set; }

        public object Value { get; set; }
        
        /// <summary>
        /// Used only for Between expressions
        /// </summary>
        public object Value2 { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.Append(Property.Name).Append(" ");

            switch (Operator)
            {
                case Operator.Eq:
                    sb.Append("=");
                    break;
                case Operator.Le:
                    sb.Append("<=");
                    break;
                case Operator.Lt:
                    sb.Append("<");
                    break;
                case Operator.Ge:
                    sb.Append(">=");
                    break;
                case Operator.Gt:
                    sb.Append(">");
                    break;
            }

            sb.Append(" ");

            sb.Append(Value);

            return sb.ToString();
        }
    }
}