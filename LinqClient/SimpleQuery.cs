﻿using System.Collections.Generic;
using System.Text;

namespace Cachealot.Linq
{
    /// <summary>
    ///     A predicate that is a conjunction or a disjunction of <see cref="AtomicExpression" />. The simplest case is a single
    ///     <see
    ///         cref="AtomicExpression" />
    /// </summary>
    public class SimpleQuery
    {
        private IList<AtomicExpression> _elements = new List<AtomicExpression>();

        public SimpleQueryType QueryType { get; set; }

        public IList<AtomicExpression> Elements
        {
            get { return _elements; }
            set { _elements = value; }
        }

        public override string ToString()
        {
            if (Elements.Count == 0)
                return "";

            var sb = new StringBuilder();

            sb.Append(Elements[0]);

            for (int i = 1; i < Elements.Count; i++)
            {
                if (QueryType == SimpleQueryType.Conjunction)
                    sb.Append(" and ");
                else if (QueryType == SimpleQueryType.Disjunction)
                    sb.Append(" or ");

                sb.Append(Elements[i]);
            }

            return sb.ToString();
        }
    }
}