#region

using System;
using System.Linq;
using System.Threading;
using CacheClient.Interface;
using CacheClient.Profiling;
using CacheClient.Queries;
using CacheTest.TestData;
using Channel;

#endregion

namespace TestClient
{
    internal class Program
    {
        private static readonly Profiler _profiler = new Profiler();

        private static void Main(string[] args)
        {
            try
            {
                int port = 4488;
                if (args.Length >= 1)
                {
                    int customPort;
                    if (int.TryParse(args[0], out customPort))
                    {
                        port = customPort;
                    }
                }

                string server = "localhost";
                if (args.Length > 1)
                    server = args[1];

                Console.WriteLine("connecting to server {0} on port{1}", server, port);

                var client = new CacheClient.Core.CacheClient();
                var channel = new TcpClientChannel(new TcpClientPool(1, 1, server, port));

                client.Channel = channel;

                client.RegisterTypeIfNeeded(typeof(CacheableTypeOk));

                _profiler.Logger = new ProfileOutput(Console.Out);

                client.Put(new CacheableTypeOk(5000, 5001, "aaa", DateTime.Now, 1500));


                //call once to exclude JIT time
                _profiler.StartMany("Put");
                var builder = new QueryBuilder(typeof(CacheableTypeOk));
                var orQuery = builder.GetMany("UniqueKey = 115");
                client.RemoveMany(orQuery);

                Console.WriteLine("PUT test");
                TestPut(client);

                Console.WriteLine("REMOVE test");

                TestRemove(client, orQuery);

                Console.WriteLine("FEED test");

                Feed(client);

                Console.WriteLine("GET MANY test");
                GetMany(client);
            }
            catch (CacheException cacheException)
            {
                Console.WriteLine(cacheException);
            }
        }

        private static void TestRemove(CacheClient.Core.CacheClient client, OrQuery orQuery)
        {
            _profiler.StartMany("Remove");
            for (int i = 0; i < 100; i++)
            {
                if (i % 10 == 0)
                {
                    Console.Write(".");
                }

                _profiler.StartOne();

                client.RemoveMany(orQuery);
                _profiler.EndOne();

                //else
                //{
                //    QueryBuilder builder = new QueryBuilder(typeof(CacheableTypeOk));
                //    OrQuery orQuery = builder.GetMany("IndexKeyValue > 1499");

                //    _profiler.Start("GetMany");
                //    IList <CacheableTypeOk> result = client.GetMany<CacheableTypeOk>(orQuery);
                //    _profiler.End();
                //    Console.WriteLine("found {0} items", result.Count);

                //}
            }

            _profiler.EndMany();
        }

        private static void TestPut(CacheClient.Core.CacheClient client)
        {
            for (int i = 0; i < 100; i++)
            {
                if (i % 10 == 0)
                {
                    Console.Write(".");
                }

                _profiler.StartOne();

                client.Put(new CacheableTypeOk(i, 100 + i, "aaa", DateTime.Now, 1500));
                _profiler.EndOne();
            }

            _profiler.EndMany();
        }

        private static void Feed(CacheClient.Core.CacheClient client)
        {
            _profiler.Start("feeding 10000 objects");

            var session = client.BeginFeed<TradeLike>(50, false);

            var random = new Random();
            for (int i = 0; i < 10000; i++)
            {
                var newItem = new TradeLike(i, 1000 + i, "aaa", new DateTime(2009, 10, 10), random.Next(1000));
                client.Add(session, newItem);
            }

            client.EndFeed<TradeLike>(session);

            _profiler.End();
        }

        private static void GetMany(CacheClient.Core.CacheClient client)
        {
            var builder = new QueryBuilder(typeof(TradeLike));
            var query = builder.GetMany("Nominal < 500");


            //JIT here
            client.GetMany<TradeLike>(query);

            //profile here
            _profiler.Start("GetMany sync");
            client.GetMany<TradeLike>(query).ToList();
            _profiler.End();


            //JIT here
            {
                var evt = new ManualResetEvent(false);

                client.AsyncGetMany(query, delegate(TradeLike like, int item, int total)
                {
                    if (item == total)
                    {
                        evt.Set();
                    }
                }, delegate { });
                evt.WaitOne();
            }

            //prfile here
            {
                _profiler.Start("GetMany async");

                var evt = new ManualResetEvent(false);

                int itemCount = 0;
                client.AsyncGetMany(query, delegate(TradeLike like, int item, int total)
                {
                    itemCount++;
                    if (item == total)
                    {
                        evt.Set();
                    }
                }, delegate { });
                evt.WaitOne();

                Console.WriteLine("Found {0} objects", itemCount);
                _profiler.End();
            }
        }
    }
}