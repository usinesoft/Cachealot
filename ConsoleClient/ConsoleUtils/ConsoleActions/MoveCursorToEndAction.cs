﻿using System;

namespace ConsoleClient.ConsoleUtils.ConsoleActions
{
    public class MoveCursorToEndAction : IConsoleAction
    {
        public void Execute(IConsole console, ConsoleKeyInfo consoleKeyInfo)
        {
            console.CursorPosition = console.CurrentLine.Length;
        }
    }
}