﻿using System;

namespace ConsoleClient.ConsoleUtils.ConsoleActions
{
    public interface IConsoleAction
    {
        void Execute(IConsole console, ConsoleKeyInfo consoleKeyInfo);
    }
}