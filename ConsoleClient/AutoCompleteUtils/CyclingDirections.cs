﻿namespace ConsoleClient.AutoCompleteUtils
{
    public enum CyclingDirections
    {
        Forward,
        Backward
    }
}