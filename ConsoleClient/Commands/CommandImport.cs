using System;
using CacheClient.Core;
using CacheClient.Interface;

namespace ConsoleClient.Commands
{
    public class CommandImport : CommandBase
    {
        public override ICacheClient TryExecute(ICacheClient client)
        {
            if (!CanExecute)
            {
                return client;
            }

            if (Params.Count != 1)
            {
                Logger.CommandLogger.WriteError("please specify a directory containing database dump(s)");
            }
            else
            {
                try
                {
                    client.ImportDump(Params[0]);
                    Logger.Write("Database successfully imported");
                }
                catch (Exception e)
                {
                    Logger.WriteEror("error importing database data:" + e.Message);

                }
            }

            return client;
        }
    }
}