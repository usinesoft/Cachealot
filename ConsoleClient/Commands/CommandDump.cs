using System;
using CacheClient.Core;
using CacheClient.Interface;

namespace ConsoleClient.Commands
{
    /// <summary>
    /// Save all the database into a directory
    /// Usage: 
    /// - dump directory (usually a network share as it needs to be visible by all servers)    
    /// </summary>
    public class CommandDump : CommandBase
    {
        public override ICacheClient TryExecute(ICacheClient client)
        {
            if (!CanExecute)
            {
                return client;
            }

            if (Params.Count != 1)
            {
                Logger.CommandLogger.WriteError("please specify a dump directory");
            }
            else
            {
                try
                {
                    client.Dump(Params[0]);
                    Logger.Write("Database successfully saved");
                }
                catch (Exception e)
                {
                    Logger.WriteEror("error saving database:" + e.Message);
                    
                }
            }

            return client;
        }
    }
}