using System;
using CacheClient.Interface;

namespace ConsoleClient.Commands
{
    public class CommandStop : CommandBase
    {
        public override ICacheClient TryExecute(ICacheClient client)
        {
            if (!CanExecute) return client;


            try
            {
                client.Stop(false);
            }
            catch (Exception)
            {
                // ignored
            }


            return client;
        }
    }
}