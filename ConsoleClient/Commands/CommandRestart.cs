using System;
using CacheClient.Interface;

namespace ConsoleClient.Commands
{
    public class CommandRestart : CommandBase
    {
        public override ICacheClient TryExecute(ICacheClient client)
        {
            if (!CanExecute) return client;


            try
            {
                client.Stop(true);
            }
            catch (Exception)
            {
                // ignored
            }


            return client;
        }
    }
}