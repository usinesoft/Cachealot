using System;
using CacheClient.Interface;

namespace ConsoleClient.Commands
{
    
    public class CommandReadOnly : CommandBase
    {
        
        public override ICacheClient TryExecute(ICacheClient client)
        {
            if (!CanExecute) return client;


            try
            {
                client.SetReadonlyMode(false);
            }
            catch (Exception)
            {
                // ignored
            }


            return client;
        }
    }


    public class CommandReadWrite : CommandBase
    {

        public override ICacheClient TryExecute(ICacheClient client)
        {
            if (!CanExecute) return client;


            try
            {
                client.SetReadonlyMode(true);
            }
            catch (Exception)
            {
                // ignored
            }


            return client;
        }
    }
}