﻿#region

using System;
using System.Net.Sockets;

#endregion

namespace Channel
{
    public class TcpClientPool : PoolStrategy<TcpClient>
    {
        private readonly string _host;
        private readonly int _port;

        public TcpClientPool(int poolCapacity, int preloaded, string host, int port) : base(poolCapacity)
        {
            _host = host;
            _port = port;

            Preload(preloaded);
        }


        protected override TcpClient GetShinyNewResource()
        {
            try
            {
                return new TcpClient(_host, _port) {Client = {NoDelay = true}};
            }
            catch (Exception)
            {
                //by returning null we notify the pool that the external connection
                //provider is not available any more
                return null;
            }
        }

        protected override bool IsStillValid(TcpClient tcp)
        {
            
            if (tcp == null)
                return false;



            try
            {
                var stream = tcp.GetStream();
                stream.WriteByte(Consts.PingCookie); // ping 
                var pingAnswer = stream.ReadByte();

                return pingAnswer == Consts.PingCookie;

            }
            catch (Exception )
            {
                return false;
            }
            
        }

        protected override void Release(TcpClient resource)
        {
            resource.Client.Close();
            resource.Close();
        }
    }
}