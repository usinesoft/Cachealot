using System;
using System.Collections.Generic;
using System.Data;
using Cachealot.Server;

namespace PerfTest
{
    class SchedulerPerfTest
    {
        private const int DATA_SIZE = 100000;
        private readonly LockFreeScheduler<object> _scheduler;

        private readonly List<int> _sharedResource;

        private readonly int _tasks;

        public SchedulerPerfTest(int threads, int tasks)
        {
            _tasks = tasks;
            _sharedResource = new List<int>();
            _scheduler = new LockFreeScheduler<object>(threads);
        }


        public void DoWithoutScheduler()
        {
            for (int i = 0; i < _tasks; i++)
            {
                if (i % 10 == 0)
                {
                    WriteAccessTask();
                }
                else
                {
                    ReadOnlyTask();
                }
            }
        }

        public void DoWithScheduler()
        {
            _scheduler.Start();
            for (int i = 0; i < _tasks; i++)
            {
                //one write access for none read-only accesses
                //the write taskis clearing theitems in the list, filling it with data and sorting
                //Fist one is a write access task( needed to initialize data)
                if (i % 10 == 0)
                {
                    WorkItem<object> writeWorkitem = new WorkItem<object>(
                        true,
                        null,
                        delegate { WriteAccessTask(); }
                    );

                    _scheduler.ProcessTask(writeWorkitem);
                }
                else //read-only access (check that data is allways sorted)
                {
                    WorkItem<object> writeWorkitem = new WorkItem<object>(
                        false,
                        null,
                        delegate { ReadOnlyTask(); }
                    );

                    _scheduler.ProcessTask(writeWorkitem);
                }
            }

            _scheduler.Stop();
        }

        private void ReadOnlyTask()
        {
            if (_sharedResource.Count != DATA_SIZE)
                throw new DataException("the list is not completely filled, probably race condition");

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < DATA_SIZE - 1; j++)
                {
                    if (_sharedResource[j] > _sharedResource[j + 1])
                        throw new DataException("the list is not sorted, probably race condition");
                }
            }
        }

        private void WriteAccessTask()
        {
            Random rand =
                new Random(Environment.TickCount);
            _sharedResource.Clear();

            for (int j = 0; j < DATA_SIZE; j++)
            {
                _sharedResource.Add(rand.Next());
            }

            _sharedResource.Sort();
        }

        public void DoReadOnlyWithScheduler()
        {
            _scheduler.Start();

            //cal oncce to initialize data structure
            WriteAccessTask();

            for (int i = 0; i < _tasks; i++)
            {
                WorkItem<object> writeWorkitem = new WorkItem<object>(
                    false,
                    null,
                    delegate { ReadOnlyTask(); }
                );

                _scheduler.ProcessTask(writeWorkitem);
            }

            _scheduler.Stop();
        }

        public void DoReadOnlyWithoutScheduler()
        {
            WriteAccessTask();

            for (int i = 0; i < _tasks; i++)
            {
                ReadOnlyTask();
            }
        }
    }
}