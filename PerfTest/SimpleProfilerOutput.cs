using System.IO;
using CacheClient;
using CacheClient.Profiling;

namespace PerfTest
{
    internal class SimpleProfilerOutput : IProfilerLogger
    {
        private readonly TextWriter _writer;

        public SimpleProfilerOutput(TextWriter writer)
        {
            _writer = writer;
        }

        public bool IsProfilingActive { get; set; } = true;

        #region IProfilerLogger Members

        public void Write(string actionName, ActionType actionType, string format, params object[] parameters)
        {
            if ((actionType == ActionType.EndSingle) || (actionType == ActionType.EndMultiple))
            {
                string msg = string.Format(format, parameters);
                string line = actionName + ": " + msg;
                _writer.WriteLine(line);
            }
        }

        #endregion
    }
}