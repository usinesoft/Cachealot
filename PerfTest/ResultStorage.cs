using System;
using System.Collections.Generic;
using System.Xml;
using CacheClient.Profiling;

namespace PerfTest
{
    internal class ResultStorage
    {
        private readonly Dictionary<string, double> _currentTimeByAction = new Dictionary<string, double>();
        private readonly Dictionary<string, double> _referenceTimeByAction = new Dictionary<string, double>();


        public ResultStorage()
        {
            try
            {
                loadRefSet("refset.xml");
            }
            catch (Exception)
            {
            }
        }

        public void Add(ProfilingData profilingData)
        {
            double measure = profilingData.IsMultiple
                ? profilingData.AvgTimeMiliseconds
                : profilingData.TotalTimeMiliseconds;

            _currentTimeByAction[profilingData.Action] = measure;
        }

        private void loadRefSet(string fileName)
        {
            var doc = new XmlDocument();
            doc.Load(fileName);
            var actionNodes = doc.SelectNodes("//action");
            foreach (XmlNode actionNode in actionNodes)
            {
                string name = actionNode.SelectSingleNode("@name").Value;
                string duration = actionNode.SelectSingleNode("@timeInMs").Value;
                _referenceTimeByAction[name] = double.Parse(duration);
            }
        }

        private void saveCurrentSet(string fileName)
        {
            var doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("actions"));

            foreach (var keyValuePair in _currentTimeByAction)
            {
                var actionElement = doc.CreateElement("action");

                {
                    var nameAttribute = doc.CreateAttribute("name");
                    nameAttribute.Value = keyValuePair.Key;
                    actionElement.Attributes.Append(nameAttribute);
                }

                {
                    var valueAttr = doc.CreateAttribute("timeInMs");
                    valueAttr.Value = keyValuePair.Value.ToString("F4");
                    actionElement.Attributes.Append(valueAttr);
                }

                doc.DocumentElement.AppendChild(actionElement);
            }

            doc.Save(fileName);
        }

        public void Save()
        {
            saveCurrentSet("results.xml");
        }

        public void ReportToConsole()
        {
            var colorBefore = Console.ForegroundColor;

            foreach (var keyValuePair in _currentTimeByAction)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(keyValuePair.Key);
                Console.CursorLeft = 2;
                Console.Write(keyValuePair.Value.ToString("F4"));

                if (_referenceTimeByAction.ContainsKey(keyValuePair.Key))
                {
                    double refvalue = _referenceTimeByAction[keyValuePair.Key];
                    int diffPercent = (int) (((keyValuePair.Value - refvalue) / refvalue) * 100);
                    if (diffPercent > 0)
                    {
                        Console.CursorLeft = 12;
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("{0}%", diffPercent);
                    }
                    else if (diffPercent < 0)
                    {
                        Console.CursorLeft = 12;
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine("{0}%", diffPercent);
                    }
                    else
                    {
                        Console.WriteLine();
                    }
                }
            }

            Console.ForegroundColor = colorBefore;
        }
    }
}