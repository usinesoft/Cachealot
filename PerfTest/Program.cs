#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Cachealot.Server;
using CacheClient.Core;
using CacheClient.Interface;
using CacheClient.Messages;
using CacheClient.Profiling;
using CacheClient.Queries;
using CacheTest.TestData;
using Channel;
using ServerConfig = Cachealot.Server.ServerConfig;

#endregion

namespace PerfTest
{
    internal class Program
    {
        private static readonly Profiler _profiler = new Profiler();
        private static readonly ResultStorage _storage = new ResultStorage();

        private static void Main(string[] args)
        {
            int bits = IntPtr.Size * 8;

            Console.WriteLine("========================================================================");
            Console.WriteLine("|                                                                      |");
            Console.WriteLine("|      Testing CacheALot components on a {0} bits machine               |", bits);
            Console.WriteLine("|                                                                      |");
            Console.WriteLine("========================================================================");
            Console.WriteLine();


            _profiler.Logger = new SimpleProfilerOutput(Console.Out);

            testOrderedIndex();
            GC.Collect();

            testDictionaryIndex();
            GC.Collect();

            testClientServer(false);
            GC.Collect();

            testClientServer(true);
            GC.Collect();

            multithreadedTestClientServer();
            GC.Collect();

            testScheduler();

            _storage.Save();

            _storage.ReportToConsole();
        }

        private static void testScheduler()
        {
            Console.WriteLine("========================================================================");
            Console.WriteLine("testing the lock free scheduler ");
            Console.WriteLine("========================================================================");

            var test = new SchedulerPerfTest(2, 1000);

            ///////////////////////////////////////////////////////////////////////////////////////////////
            ////execute once to JIT
            test.DoWithoutScheduler();
            test.DoWithScheduler();


            //now profiled execution
            _profiler.Start("1000 tasks sequentially (10% write/90% readonly)");
            test.DoWithoutScheduler();
            _storage.Add(_profiler.End());


            _profiler.Start("1000 tasks on 2 threads (10% write/90% readonly)");
            test.DoWithScheduler();
            _storage.Add(_profiler.End());

            test = new SchedulerPerfTest(4, 1000);

            _profiler.Start("1000 tasks on 4 threads (10% write/90% readonly)");
            test.DoWithScheduler();
            _storage.Add(_profiler.End());

            test = new SchedulerPerfTest(8, 1000);

            _profiler.Start("1000 tasks on 8 threads (10% write/90% readonly)");
            test.DoWithScheduler();
            _storage.Add(_profiler.End());

            /////////////////////////////////////////////////////////////////////////////////////////////

            //execute once to JIT
            test.DoReadOnlyWithoutScheduler();
            test.DoReadOnlyWithScheduler();


            //now profiled execution
            _profiler.Start("1000 tasks sequentially (100% readonly)");
            test.DoReadOnlyWithoutScheduler();
            _storage.Add(_profiler.End());


            _profiler.Start("1000 tasks on 2 threads (100% readonly)");
            test.DoReadOnlyWithScheduler();
            _storage.Add(_profiler.End());

            test = new SchedulerPerfTest(4, 1000);

            _profiler.Start("1000 tasks on 4 threads (100% readonly)");
            test.DoReadOnlyWithScheduler();
            _storage.Add(_profiler.End());

            test = new SchedulerPerfTest(8, 1000);

            _profiler.Start("1000 tasks on 8 threads (100% readonly)");
            test.DoReadOnlyWithScheduler();
            _storage.Add(_profiler.End());
        }


        private static void testClientServer(bool enableEviction)
        {
            Console.WriteLine("========================================================================");
            if (enableEviction)
                Console.WriteLine(" test in-process client server (single thread) LRU eviction");
            else
                Console.WriteLine(" test in-process client server (single thread) no eviction");
            Console.WriteLine("========================================================================");

            var client = new CacheClient.Core.CacheClient();
            var channel = new InProcessChannel();
            client.Channel = channel;

            var cfg = new ServerConfig();
            var cfgType = new ServerDatastoreConfig();
            if (enableEviction)
            {
                cfgType.Eviction.Type = EvictionType.LessRecentlyUsed;
                cfgType.Eviction.LruMaxItems = 150000;
                cfgType.Eviction.LruEvictionCount = 1500;
            }

            cfgType.FullTypeName = typeof(CacheableTypeOk).FullName;
            cfg.ConfigByType.Add(cfgType.FullTypeName, cfgType);
            var server = new Server(cfg) {Channel = channel};

            
            server.Start();
            server.WaitForServerInitialization();


            var builder = new QueryBuilder(typeof(CacheableTypeOk));

            ////////////////////////////////////////////////////////////////////////
            //first query: single test on index value

            var q = builder.GetMany("IndexKeyValue < 500005");

            //do it once to force the JIT
            IList<CacheableTypeOk> itemsReloaded = client.GetMany<CacheableTypeOk>(q).ToList();

            _profiler.StartMany("select * from CacheableTypeOk where IndexKeyValue < 500005");

            for (int i = 0; i < 10; i++)
            {
                _profiler.StartOne();
                itemsReloaded = client.GetMany<CacheableTypeOk>(q).ToList();
                _profiler.EndOne();
            }

            _storage.Add(_profiler.EndMany());


            Console.WriteLine("found {0} items", itemsReloaded.Count);

            ////////////////////////////////////////////////////////////////////////
            //first query: single test on index value
            var q1 = builder.GetMany("IndexKeyValue < 500005", "IndexKeyDate = 20100101");

            //do it once to force the JIT
            itemsReloaded = client.GetMany<CacheableTypeOk>(q1).ToList();

            _profiler.StartMany(
                "select * from CacheableTypeOk where IndexKeyValue < 500005 AND IndexKeyDate = 20100101");

            for (int i = 0; i < 10; i++)
            {
                _profiler.StartOne();
                itemsReloaded = client.GetMany<CacheableTypeOk>(q1).ToList();
                _profiler.EndOne();
            }

            _storage.Add(_profiler.EndMany());

            Console.WriteLine("found {0} items", itemsReloaded.Count);

            server.Stop();
        }


        private static void multithreadedTestClientServer()
        {
            Console.WriteLine("========================================================================");
            Console.WriteLine(" test in-process client server (multi thread)");
            Console.WriteLine("========================================================================");

            Semaphore _requestsFinished;

            var client = new CacheClient.Core.CacheClient();
            var channel = new InProcessChannel();
            client.Channel = channel;
            var server = new Server(new ServerConfig());
            server.Channel = channel;

            server.Start();

            client.RegisterTypeIfNeeded(typeof(CacheableTypeOk));

            //first load data 
            for (int i = 0; i < 100000; i++)
            {
                var item = new CacheableTypeOk(i, 10000 + i, "aaa", new DateTime(2010, 10, 10), 1500 + i);
                client.Put(item);
            }

            const int REQUESTS = 10;

            string action = string.Format("{0} times GetMany with multiple threads", REQUESTS);
            _profiler.Start(action);

            _requestsFinished = new Semaphore(0, 10);


            //paralel requests
            int itemsFound = 0;
            for (int i = 0; i < REQUESTS; i++)
            {
                ThreadPool.QueueUserWorkItem(delegate
                {
                    //reload both items by folder name
                    IList<CacheableTypeOk> items =
                        client.GetMany<CacheableTypeOk>("IndexKeyValue > 1700")
                            .ToList();
                    //if (items.Count != 200)
                    //    throw new DataException("invalid number of items returned by GetMany()"); 

                    itemsFound = items.Count;
                    _requestsFinished.Release();
                });
            }


            for (int i = 0; i < REQUESTS; i++)
            {
                _requestsFinished.WaitOne();
            }

            _storage.Add(_profiler.End());

            Console.WriteLine("found {0} items", itemsFound);


            action = string.Format("{0} times GetMany sequentially", REQUESTS);
            _profiler.Start(action);

            //the same requests sequentially
            for (int i = 0; i < REQUESTS; i++)
            {
                //reload both items by folder name
                IList<CacheableTypeOk> items = client.GetMany<CacheableTypeOk>("IndexKeyValue > 1700").ToList();
                itemsFound = items.Count;
                //if (items.Count != 200)
                //    throw new DataException("invalid number of items returned by GetMany()");
            }

            _storage.Add(_profiler.End());
            Console.WriteLine("found {0} items", itemsFound);

            server.Stop();
        }


        private static void testDictionaryIndex()
        {
            Console.WriteLine("========================================================================");
            Console.WriteLine("test dictionary index");
            Console.WriteLine("========================================================================");

            var index2 = createAndPopulateDictionaryIndex(100000);

            TestSearchString(index2, "AHA", 100000);
            TestSearchString(index2, "BOLO", 100000);

            TestCountString(index2, "AHA", 100000);
            TestCountString(index2, "BOLO", 100000);
        }


        private static void testOrderedIndex()
        {
            Console.WriteLine("========================================================================");
            Console.WriteLine("test ordered index");
            Console.WriteLine("========================================================================");


            var typeDescription = ClientSideTypeDescription.RegisterType(typeof(CacheableTypeOk));

            KeyInfo valueKey = null;

            foreach (var keyInfo in typeDescription.IndexFields)
            {
                if (keyInfo.Name == "IndexKeyValue")
                    valueKey = keyInfo.AsKeyInfo;
            }

            IndexBase index = new OrderedIndex(valueKey);


            //_profiler.est();

            PopulateIndex(index, 100000);

            Console.WriteLine();
            Console.WriteLine("inserting 100 items");
            Console.WriteLine("-----------------------");
            TestInsert(index, 100);


            Console.WriteLine();
            Console.WriteLine("inserting 1000 items");
            Console.WriteLine("-----------------------");
            TestInsert(index, 1000);

            Console.WriteLine();
            Console.WriteLine("inserting 100 already packed items");
            Console.WriteLine("-----------------------");
            TestInsertAlreadyPacked(index, 100);


            Console.WriteLine();
            Console.WriteLine("inserting 1000 already packed items");
            Console.WriteLine("-----------------------");
            TestInsertAlreadyPacked(index, 1000);


            index.Clear();

            PopulateIndex(index, 100000, 100, 199789);

            Console.WriteLine();
            Console.WriteLine("searching for 100 items");
            Console.WriteLine("-----------------------");
            TestSearch(index, 199789, 100, 100000, QueryOperator.Eq);
            TestSearch(index, 199789, 100, 100000, QueryOperator.Lt);
            TestSearch(index, 199789, 100, 100000, QueryOperator.Le);
            TestSearch(index, 199789, 100, 100000, QueryOperator.Ge);
            TestSearch(index, 199789, 100, 100000, QueryOperator.Gt);

            index.Clear();
            PopulateIndex(index, 100000, 1000, 152);

            Console.WriteLine();
            Console.WriteLine("searching for 1000 items");
            Console.WriteLine("------------------------");
            TestSearch(index, 152, 1000, 100000, QueryOperator.Eq);
            TestSearch(index, 152, 1000, 100000, QueryOperator.Lt);
            TestSearch(index, 152, 1000, 100000, QueryOperator.Le);
            TestSearch(index, 152, 1000, 100000, QueryOperator.Ge);
            TestSearch(index, 152, 1000, 100000, QueryOperator.Gt);


            Console.WriteLine();
            Console.WriteLine("removing 1000 items");
            Console.WriteLine("------------------------");
            TestRemove(index, 152, 1000, 100000);

            Console.WriteLine();
            Console.WriteLine("counting 100 items");
            Console.WriteLine("-----------------------");
            TestCount(index, 199789, 100, 100000, QueryOperator.Eq);
            TestCount(index, 199789, 100, 100000, QueryOperator.Lt);
            TestCount(index, 199789, 100, 100000, QueryOperator.Le);
            TestCount(index, 199789, 100, 100000, QueryOperator.Ge);
            TestCount(index, 199789, 100, 100000, QueryOperator.Gt);

            index.Clear();
            PopulateIndex(index, 100000, 1000, 152);

            Console.WriteLine();
            Console.WriteLine("counting 1000 items");
            Console.WriteLine("------------------------");
            TestCount(index, 152, 1000, 100000, QueryOperator.Eq);
            TestCount(index, 152, 1000, 100000, QueryOperator.Lt);
            TestCount(index, 152, 1000, 100000, QueryOperator.Le);
            TestCount(index, 152, 1000, 100000, QueryOperator.Ge);
            TestCount(index, 152, 1000, 100000, QueryOperator.Gt);
        }


        private static void ProfilerTest()
        {
            _profiler.Start("profiling the profiler:single");
            _storage.Add(_profiler.End());

            _profiler.StartMany("profiling the profiler:multiple");
            for (int i = 0; i < 100; i++)
            {
                _profiler.StartOne();
                _profiler.EndOne();
            }

            _storage.Add(_profiler.EndMany());
        }

        private static void TestSearch(IndexBase index, int value, int expectedCount, int totalCount,
            QueryOperator oper)
        {
            string actionName = string.Format("search {0} / {1} operator = {2}", expectedCount, totalCount, oper);

            var keyType = new KeyInfo(KeyDataType.IntKey, KeyType.ScalarIndex, "test", true);
            _profiler.StartMany(actionName);

            //JIT compiling here
            var result = index.GetMany(new List<KeyValue> {new KeyValue(value, keyType)}, oper);

            for (int j = 0; j < 15; j++)
            {
                _profiler.StartOne();
                result = index.GetMany(new List<KeyValue> {new KeyValue(value, keyType)}, oper);
                _profiler.EndOne();
            }

            _storage.Add(_profiler.EndMany());

            Console.WriteLine("found {0} items", result.Count);
        }


        private static void TestSearchString(IndexBase index, string value, int totalCount)
        {
            string actionName = string.Format("search for key {0} in {1} elements", value, totalCount);
            var keyType = new KeyInfo(KeyDataType.IntKey, KeyType.ScalarIndex, "test", true);

            _profiler.StartMany(actionName);

            //JIT compiling here

            var result = index.GetMany(new List<KeyValue> {new KeyValue(value, keyType)});

            for (int j = 0; j < 15; j++)
            {
                _profiler.StartOne();
                result = index.GetMany(new List<KeyValue> {new KeyValue(value, keyType)});
                _profiler.EndOne();
            }

            _storage.Add(_profiler.EndMany());

            Console.WriteLine("found {0} items", result.Count);
        }

        private static void TestCountString(IndexBase index, string value, int totalCount)
        {
            string actionName = string.Format("count items for key {0} in {1} elements", value, totalCount);
            var keyType = new KeyInfo(KeyDataType.IntKey, KeyType.ScalarIndex, "test", true);

            _profiler.StartMany(actionName);

            //JIT compiling here

            int result = index.GetCount(new List<KeyValue> {new KeyValue(value, keyType)});

            for (int j = 0; j < 15; j++)
            {
                _profiler.StartOne();
                result = index.GetCount(new List<KeyValue> {new KeyValue(value, keyType)});
                _profiler.EndOne();
            }

            _storage.Add(_profiler.EndMany());

            Console.WriteLine("found {0} items", result);
        }

        private static void TestRemove(IndexBase index, int value, int expectedCount, int totalCount)
        {
            string actionName = string.Format("removing {0} / {1}", expectedCount, totalCount);

            var keyType = new KeyInfo(KeyDataType.IntKey, KeyType.ScalarIndex, "test", true);

            var result = index.GetMany(new List<KeyValue> {new KeyValue(value, keyType)});
            Console.WriteLine("found {0} items", result.Count);

            _profiler.StartMany(actionName);


            foreach (var cachedObject in result)
            {
                _profiler.StartOne();
                index.RemoveOne(cachedObject);
                _profiler.EndOne();
            }

            _storage.Add(_profiler.EndMany());


            Console.WriteLine("found {0} items", index.GetCount(new List<KeyValue> {new KeyValue(value, keyType)}));
        }

        private static void TestCount(IndexBase index, int value, int expectedCount, int totalCount, QueryOperator oper)
        {
            string actionName = string.Format("count {0} / {1} operator = {2}", expectedCount, totalCount, oper);
            var keyType = new KeyInfo(KeyDataType.IntKey, KeyType.ScalarIndex, "test", true);
            _profiler.StartMany(actionName);

            //JIT compiling here
            int count = index.GetCount(new List<KeyValue> {new KeyValue(value, keyType)}, oper);

            for (int j = 0; j < 15; j++)
            {
                _profiler.StartOne();
                count = index.GetCount(new List<KeyValue> {new KeyValue(value, keyType)}, oper);
                _profiler.EndOne();
            }

            _storage.Add(_profiler.EndMany());

            Console.WriteLine("found {0} items", count);
        }

        private static void TestInsert(IndexBase index, int items)
        {
            _profiler.StartMany("index insert");
            //JIT compiling here
            index.Put(CachedObject.Pack(new CacheableTypeOk(4444, 106, "A", DateTime.Now, 44444)));


            for (int i = 0; i < items; i++)
            {
                _profiler.StartOne();
                index.Put(CachedObject.Pack(new CacheableTypeOk(i, 106, "A", DateTime.Now, i * 5)));
                _profiler.EndOne();
            }


            _storage.Add(_profiler.EndMany());
        }

        private static void TestInsertAlreadyPacked(IndexBase index, int items)
        {
            var packed = new CachedObject[items];

            for (int i = 0; i < items; i++)
            {
                packed[i] = CachedObject.Pack(new CacheableTypeOk(i, 106, "A", DateTime.Now, i * 5));
            }

            _profiler.StartMany("index insert");
            for (int i = 0; i < items; i++)
            {
                _profiler.StartOne();
                index.Put(packed[i]);
                _profiler.EndOne();
            }


            _storage.Add(_profiler.EndMany());
        }

        private static void PopulateIndex(IndexBase index, int items)
        {
            //_profiler.StartMany("index filling");

            index.BeginFill();
            for (int i = 0; i < items; i++)
            {
                //_profiler.StartOne();
                index.Put(CachedObject.Pack(new CacheableTypeOk(i, 106, "A", DateTime.Now, i * 3)));
                //_profiler.EndOne();
            }

            index.EndFill();

            //_storage.Add(_profiler.EndMany());
        }

        /// <summary>
        ///     populate the index and add a range of items having the specified value in the Value
        /// </summary>
        /// <param name="index"></param>
        /// <param name="items"></param>
        /// <param name="itemsValue"></param>
        /// <param name="value"></param>
        private static void PopulateIndex(IndexBase index, int items, int itemsValue, int value)
        {
            //_profiler.StartMany("index filling");

            index.BeginFill();
            for (int i = 0; i < items; i++)
            {
                //_profiler.StartOne();
                index.Put(CachedObject.Pack(new CacheableTypeOk(i, 106, "A", DateTime.Now, i * 3)));
                //_profiler.EndOne();
            }

            for (int i = 0; i < itemsValue; i++)
            {
                //_profiler.StartOne();
                index.Put(CachedObject.Pack(new CacheableTypeOk(i + items, 106, "A", DateTime.Now, value)));
                //_profiler.EndOne();
            }

            index.EndFill();

            //_storage.Add(_profiler.EndMany());
        }

        private static IndexBase createAndPopulateDictionaryIndex(int items)
        {
            var typeDescription = ClientSideTypeDescription.RegisterType(typeof(CacheableTypeOk));

            KeyInfo folderKey = null;

            foreach (var keyInfo in typeDescription.IndexFields)
            {
                if (keyInfo.Name == "IndexKeyFolder")
                    folderKey = keyInfo.AsKeyInfo;
            }

            var index = IndexFactory.CreateIndex(folderKey);

            string[] folders = {"AHA", "AHI", "AHO", "BALA", "BULU", "BILI", "BOLO", "WERXES", "XERES", "ZAK"};

            ///JIT here
            index.Put(CachedObject.Pack(new CacheableTypeOk(500000, 500000 + 50, folders[0], DateTime.Now, 654654)));

            _profiler.StartMany("pupulating a dictionary index");
            for (int i = 0; i < items; i++)
            {
                _profiler.StartOne();
                index.Put(
                    CachedObject.Pack(
                        new CacheableTypeOk(i, i * 100, folders[i % folders.Length], DateTime.Now, 654654)));
                _profiler.EndOne();
            }

            _storage.Add(_profiler.EndMany());


            return index;
        }
    }
}