using System;
using System.Collections.Generic;
using CacheClient.Core;
using CacheClient.Messages;

namespace ServerPluginInterface
{
    /// <summary>
    /// Argument of the FinishedLoading event from <see cref="ILoader"/>
    /// </summary>
    public class FinishedLoadingEventArgs : EventArgs
    {
        /// <summary>
        /// List of key values for which all data was loaded
        /// </summary>
        private readonly List<KeyValue> _fullyLoadedDomains;

        /// <summary>
        /// If true all data of this type was preloaded
        /// </summary>
        private bool _fullyPreloaded;

        /// <summary>
        /// Available data as generic <see cref="CachedObject"/>
        /// </summary>
        private IList<CachedObject> _loadedData;

        /// <summary>
        /// True if the loader successfully loaded data
        /// </summary>
        private bool _success;


        public FinishedLoadingEventArgs()
        {
            _fullyLoadedDomains = new List<KeyValue>();
        }

        /// <summary>
        /// True if the loader successfully loaded data
        /// </summary>
        public bool Success
        {
            get { return _success; }
            set { _success = value; }
        }

        /// <summary>
        /// List of key values for wich all data was loaded
        /// </summary>
        public IList<KeyValue> FullyLoadedDomains
        {
            get { return _fullyLoadedDomains; }
        }

        /// <summary>
        /// If true all data of this type was preloaded
        /// </summary>
        public bool FullyPreloaded
        {
            get { return _fullyPreloaded; }
            set { _fullyPreloaded = value; }
        }

        /// <summary>
        /// Available data as generic <see cref="CachedObject"/>
        /// </summary>
        public IList<CachedObject> LoadedData
        {
            get { return _loadedData; }
            set { _loadedData = value; }
        }

        public TypeDescription DataType { get; set; }
    }
}