using System;

namespace ServerPluginInterface
{
    /// <summary>
    /// A loader is a type of plugin which preloades data to the cache when the server is started
    /// </summary>
    public interface ILoader
    {
        /// <summary>
        /// Start loading data (data loading can be asynchronous)
        /// </summary>
        void StartLoading();

        /// <summary>
        /// Fired when all the data was loaded
        /// </summary>
        event EventHandler<FinishedLoadingEventArgs> FinishedLoading;
    }
}