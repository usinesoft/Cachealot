﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Cachealot")]
[assembly: AssemblyDescription("Client for the Cachealot DB nosql database")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("USINESOFT")]
[assembly: AssemblyProduct("Cachealot")]
[assembly: AssemblyCopyright("Copyright © Usinesoft 2010-2018")]
[assembly: AssemblyTrademark("Cachealot DB")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3c9c44e3-8d7e-4571-92c7-3de20805a89d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]