using System.Collections;
using System.Collections.Generic;
using CacheClient.Core;
using CacheClient.Interface;

namespace Cachealot.Linq
{

    /// <summary>
    /// A transaction contains Put and Delete operations. They will be executed in an ACID transaction
    /// </summary>
    public class Transaction
    {
        private readonly ICacheClient _client;
        readonly HashSet<KeyValue> _itemsDoDelete = new HashSet<KeyValue>();

        internal Transaction(IDictionary<string, ClientSideTypeDescription> typeDescriptions, ICacheClient client)
        {
            _client = client;
            TypeDescriptions = typeDescriptions;
        }

        internal IDictionary<string, ClientSideTypeDescription> TypeDescriptions { get; }

        internal Dictionary<KeyValue, CachedObject> ItemsToPut { get; } = new Dictionary<KeyValue, CachedObject>();


        public void Put<T>(T item)
        {
            var packed = Pack(item);

            ItemsToPut[packed.PrimaryKey] = packed;
        }

        

        public void Delete<T>(T item)
        {
            var packed = Pack(item);

            _itemsDoDelete.Add(packed.PrimaryKey);
        }


        public void Commit()
        {
            _client.ExecuteTransaction(ItemsToPut.Values, _itemsDoDelete);
        }


        private CachedObject Pack<T>(T item)
        {
            var name = typeof(T).FullName;
            if (TypeDescriptions.ContainsKey(name))
            {
                TypeDescriptions[name] = ClientSideTypeDescription.RegisterType<T>();
            }

            var packed = CachedObject.Pack(item, TypeDescriptions[name]);
            return packed;
        }
    }
}