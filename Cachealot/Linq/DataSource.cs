using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CacheClient.Core;
using CacheClient.Interface;
using Remotion.Linq;
using Remotion.Linq.Parsing.Structure;


namespace Cachealot.Linq
{
    /// <summary>
    /// All data access and update methods. Implements a powerful linq provider 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DataSource<T> : QueryableBase<T>
    {
        private readonly ICacheClient _client;
        private readonly ClientSideTypeDescription _typeDescription;

        internal DataSource(ICacheClient client, ClientSideTypeDescription typeDescription) 
            : base(QueryParser.CreateDefault(), new QueryExecutor(client, typeDescription.AsTypeDescription))
        {
            
            _client = client;
            
            _typeDescription = typeDescription;
        }

        
        public DataSource(IQueryParser queryParser, IQueryExecutor executor)
            : base(new DefaultQueryProvider(typeof(DataSource<>), queryParser, executor))
        {
        }

        public DataSource(IQueryProvider provider, Expression expression)
            : base(provider, expression)
        {
        }


        /// <summary>
        /// Get one item by primary key. Returns null if none
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        public T this[object primaryKey] => _client.GetOne<T>(primaryKey);


        /// <summary>
        /// Update or insert an object
        /// </summary>
        /// <param name="item"></param>
        public void Put(T item)
        {
            _client.Put(item);
        }

        /// <summary>
        /// Delete one item by primary key
        /// </summary>
        /// <param name="item"></param>
        public void Delete(T item)
        {
            var packed = CachedObject.Pack(item, _typeDescription);
            _client.Remove<T>(packed.PrimaryKey);
        }


        /// <summary>
        /// Remove all the items matching the query
        /// This method is transactional on a single node cluster
        /// </summary>
        /// <param name="where"></param>
        public void DeleteMany(Expression<Func<T, bool>> where)
        {

            QueryVisitor visitor = new QueryVisitor(_typeDescription.AsTypeDescription);

            Expression<Func<IQueryable<T>>> expr = () => this.Where(where);

            var queryModel = QueryParser.CreateDefault().GetParsedQuery(expr.Body);
            visitor.VisitQueryModel(queryModel);

            _client.RemoveMany(visitor.RootExpression);

        }

        
        /// <summary>
        /// Update or insert a collection of objects. Items are fed by package to optimize network transport.
        /// For new items an optimized bulk insert algorithm is used
        /// This method is transactional on a single node cluster
        /// </summary>
        /// <param name="items"></param>
        public void FeedMany(IEnumerable<T> items)  
        {
           _client.FeedMany(items);
        }

       
    }
}