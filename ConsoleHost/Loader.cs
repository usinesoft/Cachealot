using System;
using System.Collections.Generic;
using CacheClient.Core;
using CacheClient.Messages;
using CacheTest.TestData;
using ServerPluginInterface;

namespace ConsoleHost
{
    /// <summary>
    /// Fake loader plugin
    /// </summary>
    class Loader : ILoader
    {
        public void StartLoading()
        {
            List<CachedObject> items = new List<CachedObject>();

            TypeDescription dataType =
                ClientSideTypeDescription.RegisterType(typeof(TradeLike)).AsTypeDescription;

            string[] folders = {"aaa", "bbb", "ccc", "dddd"};

            for (int i = 0; i < 100000; i++)
            {
                TradeLike newItem = new TradeLike(i, i + 10000, folders[i % 4],
                    new DateTime(2010, i % 11 + 1, i % 27 + 1), 500000 + i % 10);
                items.Add(CachedObject.Pack(newItem));
            }

            FinishedLoadingEventArgs args = new FinishedLoadingEventArgs();
            args.DataType = dataType;
            args.FullyPreloaded = true;
            args.LoadedData = items;
            args.Success = true;

            if (FinishedLoading != null)
                FinishedLoading(this, args);
        }

        public event EventHandler<FinishedLoadingEventArgs> FinishedLoading;
    }
}