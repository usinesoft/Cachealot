using System;
using System.Threading;
using Cachealot.Server;
using Channel;

namespace ConsoleHost
{
    class Program
    {
        static void Main(string[] args)
        {
            int port = 4488;
            if (args.Length == 1)
            {
                int customPort;
                if (int.TryParse(args[0], out customPort))
                {
                    port = customPort;
                }
            }

            int bits = IntPtr.Size * 8;

            Console.WriteLine("========================================================================");
            Console.WriteLine("|                                                                      |");
            Console.WriteLine("|      CacheALot server on a {0} bits machine                           |", bits);
            Console.WriteLine("|                                                                      |");
            Console.WriteLine("========================================================================");
            Console.WriteLine();


            ServerConfig config;

            try
            {
                config = ServerConfig.LoadFromFile("cacheserver.xml");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can not read configuration file: {0}", ex.Message);
                return;
            }


            Server cacheServer = new Server(config);            
            TcpServerChannel listener = new TcpServerChannel();
            cacheServer.Channel = listener;
            listener.Init(config.TcpPort);
            listener.Start();


            //cacheServer.RegisterLoader(ClientSideTypeDescription.RegisterType(typeof(TradeLike)).AsTypeDescription, new Loader());
            cacheServer.Start();

            Console.WriteLine("Waiting for loader plugins...");
            //cacheServer.WaitForServerInitialization();
            Console.WriteLine("Data preloaded successfully");

            ManualResetEvent evtEndOfEternity = new ManualResetEvent(false);
            Console.WriteLine("CacheALot is open for business on port {0}", config.TcpPort);
            evtEndOfEternity.WaitOne();
        }
    }
}