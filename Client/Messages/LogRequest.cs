using CacheClient.ChannelInterface;
using ProtoBuf;

namespace CacheClient.Messages
{
    [ProtoContract]
    public class LogRequest : Request
    {
        [ProtoMember(1)] private int _linesCount;

        public LogRequest()
        {
        }

        public LogRequest(int linesCount)
        {
            LinesCount = linesCount;
        }

        public override RequestClass RequestClass
        {
            get { return RequestClass.Admin; }
        }

        public int LinesCount
        {
            get { return _linesCount; }
            set { _linesCount = value; }
        }
    }
}