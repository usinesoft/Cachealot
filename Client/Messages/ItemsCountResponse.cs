using CacheClient.ChannelInterface;
using ProtoBuf;

namespace CacheClient.Messages
{
    [ProtoContract]
    public class ItemsCountResponse : Response
    {
        [ProtoMember(1)] private int _items;

        public ItemsCountResponse()
        {
        }

        public ItemsCountResponse(int items)
        {
            _items = items;
        }

        public override ResponseType ResponseType
        {
            get { return ResponseType.Data; }
        }

        /// <summary>
        /// Number of items in the result set
        /// </summary>
        public int ItemsCount
        {
            get { return _items; }
            set { _items = value; }
        }
    }
}