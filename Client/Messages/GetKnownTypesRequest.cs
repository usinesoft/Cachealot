using CacheClient.ChannelInterface;
using ProtoBuf;

namespace CacheClient.Messages
{
    /// <summary>
    /// Ask the server for the registered types descriptions
    /// </summary>
    [ProtoContract]
    public class GetKnownTypesRequest : Request
    {
        public override RequestClass RequestClass
        {
            get { return RequestClass.Admin; }
        }
    }
}