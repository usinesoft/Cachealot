using System.Collections.Generic;
using CacheClient.ChannelInterface;
using CacheClient.Core;
using ProtoBuf;

namespace CacheClient.Messages
{

    public class TransactionRequest : Request
    {
        /// <summary>
        /// Used for protobus serialization
        /// </summary>
        public TransactionRequest()
        {
            
        }

        public TransactionRequest(List<CachedObject> itemsToPut, List<KeyValue> primaryKeysTodelete)
        {
            _itemsToPut = itemsToPut;
            _primaryKeysTodelete = primaryKeysTodelete;
        }

        [ProtoMember(1)] private List<CachedObject> _itemsToPut = new List<CachedObject>();

        [ProtoMember(2)] private List<KeyValue> _primaryKeysTodelete = new List<KeyValue>();

        public override RequestClass RequestClass  => RequestClass.DataAccess;

        public List<CachedObject> ItemsToPut
        {
            get { return _itemsToPut; }
        }
    }
}