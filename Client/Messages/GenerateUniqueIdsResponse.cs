using CacheClient.ChannelInterface;
using ProtoBuf;

namespace CacheClient.Messages
{
    [ProtoContract]
    public class GenerateUniqueIdsResponse : Response
    {
        public override ResponseType ResponseType => ResponseType.Data;

        public GenerateUniqueIdsResponse()
        {
        }

        public GenerateUniqueIdsResponse(int[] ids)
        {
            Ids = ids;
        }

     
        [ProtoMember(1)]
        public int[] Ids { get; set; }


        
    }
}