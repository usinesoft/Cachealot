using CacheClient.Core;
using CacheClient.Queries;
using ProtoBuf;

namespace CacheClient.Messages
{
    /// <summary>
    /// Remove a subset specified by a query
    /// </summary>
    [ProtoContract]
    public class RemoveManyRequest : DataRequest
    {
        [ProtoMember(1)] private readonly OrQuery _query;

        public RemoveManyRequest() : base(DataAccessType.Write, string.Empty)
        {
        }

        public RemoveManyRequest(OrQuery query)
            : base(DataAccessType.Write, query.TypeName)
        {
            _query = query;
        }

        public OrQuery Query
        {
            get { return _query; }
        }
    }
}