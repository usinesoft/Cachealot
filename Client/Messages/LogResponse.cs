using System.Collections.Generic;
using CacheClient.ChannelInterface;
using ProtoBuf;

namespace CacheClient.Messages
{
    [ProtoContract]
    public class LogResponse : Response
    {
        [ProtoMember(1)] private readonly List<ServerLogEntry> _entries = new List<ServerLogEntry>();
        [ProtoMember(2)] private ServerLogEntry _maxLockEntry;

        public IList<ServerLogEntry> Entries
        {
            get { return _entries; }
        }

        public ServerLogEntry MaxLockEntry
        {
            get { return _maxLockEntry; }
            set { _maxLockEntry = value; }
        }

        public override ResponseType ResponseType
        {
            get { return ResponseType.Data; }
        }
    }
}