using CacheClient.ChannelInterface;
using CacheClient.Core;
using CacheClient.Queries;
using ProtoBuf;

namespace CacheClient.Messages
{
    [ProtoContract]
    public class GetDescriptionRequest : DataRequest
    {
        [ProtoMember(1)] private readonly OrQuery _query;

        /// <summary>
        /// For serialization only 
        /// </summary>
        public GetDescriptionRequest() : base(DataAccessType.Read, string.Empty)
        {
        }

        public GetDescriptionRequest(OrQuery query)
            : base(DataAccessType.Read, query.TypeName)
        {
            _query = query;
        }

        public override RequestClass RequestClass
        {
            get { return RequestClass.DataAccess; }
        }

        public OrQuery Query
        {
            get { return _query; }
        }
    }
}