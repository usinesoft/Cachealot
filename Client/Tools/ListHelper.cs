﻿using System;
using System.Collections.Generic;

namespace CacheClient.Tools
{
    public static class ListHelper
    {
        /// <summary>
        /// Create a sorted list by merging an already sorted one with a new one
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sortedList"></param>
        /// <param name="unsortedList"></param>
        /// <param name="compare"></param>
        public static void InterpolateLists<T>(this List<T> sortedList, List<T> unsortedList, Comparison<T> compare)
        {
            unsortedList.Sort(compare);

            int indexLeft = 0;
            int indexRight = 0;



            if (sortedList.Count == 0)
            {
                sortedList.AddRange(unsortedList);
                return;
            }

            while (indexRight < unsortedList.Count)
            {
                var currentRight = unsortedList[indexRight];


                var currentLeft = sortedList[indexLeft];
                
                if (compare(currentRight, currentLeft) <= 0 )
                {
                    sortedList.Insert(indexLeft, currentRight);
                    indexRight++;
                    indexLeft++;
                }
                else
                {
                    if (indexLeft +1 < sortedList.Count)
                    {
                        var nextLeft = sortedList[indexLeft + 1];
                        if (compare(currentRight, nextLeft) <= 0)
                        {
                            sortedList.Insert(indexLeft + 1, currentRight);
                            indexRight++;
                            indexLeft += 2;
                        }
                        else
                        {
                            indexLeft ++;
                        }
                    }
                    else
                    {
                        sortedList.Add(currentLeft);
                        indexRight++;
                    }
                }
            }

        }

    }
}
