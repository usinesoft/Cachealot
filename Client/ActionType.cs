namespace CacheClient
{
    /// <summary>
    /// Type of profiling action
    /// </summary>
    public enum ActionType
    {
        /// <summary>
        /// Start profiling for a one-shot activity
        /// </summary>
        StartSingle,

        /// <summary>
        /// End profiling for a one-shot activity
        /// </summary>
        EndSingle,

        /// <summary>
        /// Start profiling multiple instances of a loop acivity 
        /// </summary>
        StartMultiple,

        /// <summary>
        /// /// <summary>
        /// Start profiling multiple instances of a loop acivity 
        /// </summary> profiling multiple instances of a loop acivity 
        /// </summary>
        EndMultiple,

        /// <summary>
        /// Start profiling one instance of a loop acivity 
        /// </summary>
        StartOne,

        /// <summary>
        /// End profiling one instance of a loop acivity 
        /// </summary>
        EndOne
    }
}