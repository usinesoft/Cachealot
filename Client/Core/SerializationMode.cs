namespace CacheClient.Core
{
    public enum SerializationMode
    {
        Json,
        ProtocolBuffers        
    }
}