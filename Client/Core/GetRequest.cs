using CacheClient.ChannelInterface;
using CacheClient.Messages;
using CacheClient.Queries;
using ProtoBuf;

namespace CacheClient.Core
{
    [ProtoContract]
    public class GetRequest : DataRequest
    {
        [ProtoMember(1)] private readonly OrQuery _query;

        public GetRequest(OrQuery query)
            : base(DataAccessType.Read, query.TypeName)
        {
            _query = query;
        }


        /// <summary>
        /// Used only for deserialization
        /// </summary>
        public GetRequest() : base(DataAccessType.Read, string.Empty)
        {
        }

        public override RequestClass RequestClass
        {
            get { return RequestClass.DataAccess; }
        }

        public OrQuery Query
        {
            get { return _query; }
        }

        [ProtoMember(2)] public bool OnlyIfComplete { get; set; }
    }
}