﻿using System;
using CacheClient.Interface;

namespace CacheClient.Core.TypeConverters
{
    public class FloatConverter : IKeyConverter
    {
        public Type SourceType => typeof(float);

        public bool CanConvertToLong => true;

        public bool CanConvertToString => false;

        public long GetAsLong(object key)
        {
            float value = (float) key;
            return (long) (value * 10000);
        }

        public string GetAsString(object key)
        {
            throw new NotImplementedException();
        }
    }
}