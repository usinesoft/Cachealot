using System;

namespace CacheClient.Interface
{
    /// <summary>
    /// Tag for the optional unique keys
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class KeyAttribute : Attribute
    {
        /// <summary>
        /// Build a unique key having the specified data type
        /// </summary>
        /// <param name="keyDataType"></param>
        public KeyAttribute(KeyDataType keyDataType)
        {
            KeyDataType = keyDataType;
        }

        /// <summary>
        /// int or string key
        /// </summary>
        public KeyDataType KeyDataType { get; } = KeyDataType.Unknown;
    }
}