using System;
using System.Text;

namespace CacheClient.Interface
{
    /// <summary>
    /// Cache exception. May be client or server generated
    /// </summary>
    public class CacheException : Exception
    {
        /// <summary>
        /// Pure client-side exception
        /// </summary>
        /// <param name="message"></param>                
        public CacheException(string message) : this(message, "", "")
        {
        }

        /// <summary>
        /// Wrap a server exception in an exception to be thrown on the client side
        /// </summary>
        /// <param name="message"></param>
        /// <param name="serverMessage"></param>
        /// <param name="serverStack"></param>
        public CacheException(string message, string serverMessage, string serverStack) : base(message)
        {
            ServerStack = serverStack;
            ServerMessage = serverMessage;
        }

        /// <summary>
        /// The original meessage from the exception raised on the server
        /// </summary>
        public string ServerMessage { get; internal set; }

        /// <summary>
        /// The original call stack from the exception raised on the server
        /// </summary>
        public string ServerStack { get; internal set; }

        public override string Message => base.Message + "(" + ServerMessage + ")";

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Client exception:");
            sb.AppendLine(Message);
            sb.AppendLine(StackTrace);

            sb.AppendLine("Server exception:");
            sb.AppendLine(ServerMessage);
            sb.AppendLine(ServerStack);

            return sb.ToString();
        }
    }
}