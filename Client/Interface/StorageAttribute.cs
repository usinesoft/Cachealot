using System;

namespace CacheClient.Interface
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public sealed class StorageAttribute : Attribute
    {
        public StorageAttribute(bool useCompression)
        {
            UseCompression = useCompression;
        }

        public bool UseCompression { get; }
    }
}