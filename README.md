# Cachealot
The project started years ago with the goal of creating a **distributed cache** that can process **complex set queries** and that is **fully transactional** (even for updates that impact multiple nodes).

Caches with set queries (get many)  need to manage the *completion status* of their data as they may contain only a subset of a database. 

In the case of Cachealot, once data is fed-in, an explicit declaration allows to specify the subset of data available. 

A "get many" request will return data only if it can guarantee that all data that can match the query is available.

Of course it can also process simple queries by unique key as all classical caches. In this case it linearly scales up when the number of nodes increases.

It provides a **powerful linq provider**.

It achieved its goal. It is now used by some financial institutions in Paris (France)

Recently we decided to use the very poweful query engine in order to develop a **fully transactional NOSQL database**. 
We are almost there, a pre release is imminent. As of now we are observing **impressive performance**
With *one single node* around *50000 durable write (transactional) operations per second*. 
And it linearly scales up with the number of nodes if you can afford only "one stage commit" transactions. 
"Two stage commit" transactions are also available with a cost in terms of response time which is as small as possible for a distributed transaction. 


Using an original approach to distributed transactions we can guarantee **lock free distributed transactions**. 
Only the nodes effectively involved in the transaction put other operations on wait during transaction and the lock is *automatically released in case of failure*.


