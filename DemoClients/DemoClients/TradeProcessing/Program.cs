﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Cachealot.Linq;
using CacheClient.Interface;
using TradeProcessing.BusinessModel.Core;
using TradeProcessing.BusinessModel.Events;
using TradeProcessing.BusinessModel.Handlers;
using TradeProcessing.Helpers;

namespace TradeProcessing
{
    internal class Program
    {
        private static readonly ClientConfig Config = new ClientConfig
        {
            IsPersistent = true,
            Servers =
            {
                new ServerConfig
                {
                    Host = "localhost",
                    Port = 6666
                }
            }
        };

        //private static readonly ClientConfig Config = new ClientConfig
        //{
        //    IsPersistent = true,
        //    Servers =
        //    {
        //        new ServerConfig
        //        {
        //            Host = "localhost",
        //            Port = 4848
        //        },
        //        new ServerConfig
        //        {
        //            Host = "localhost",
        //            Port = 4849
        //        },
        //        new ServerConfig
        //        {
        //            Host = "localhost",
        //            Port = 4850
        //        }
        //    }
        //};


        private static IEnumerable<Trade> TradesGenerator(Trade template, Connector connector, int quantity)
        {
            

            var tid = connector.GenerateUniqueIds("trade_id", quantity);

            var cid = connector.GenerateUniqueIds("contract_id", quantity);

            

            for (var i = 0; i < quantity; i++)
            {
                var clone = template.Clone();
                clone.Id = tid[i];
                clone.ContractId = "IRD-" + cid[i];
                yield return clone;
            }
        }

        private static IEnumerable<ProductEvent> EventGenerator(ProductEvent template, Connector connector, int quantity)
        {
            
            var tid = connector.GenerateUniqueIds("event_id", quantity);
            
            for (var i = 0; i < quantity; i++)
            {
                var clone = template.Clone();
                clone.EventId = tid[i];                
                yield return clone;
            }
        }

        private static void Main(string[] args)
        {
            using (var connector = new Connector(Config))
            {
                var handler = Handlers.GetHandler("InterestRateSwap", "Create");
                handler.Connector = connector;

                (var newTrade, var create) = handler.ApplyEvent(new CreateInterestRateSwapEventinfo
                {
                    Counterparty = "GOLDMAN.LDN",
                    FixedRate = 0.05M,
                    Notional = 100000,
                    MaturityDate = DateTime.Today.AddYears(10),
                    Portfolio = "SWEUR",
                    Owner = "MOI",
                    Tenor = "3M",
                    VariableRate = "EONIA"
                });


                var events = connector.DataSource<ProductEvent>();
                var trades = connector.DataSource<Trade>();

                trades.Put(newTrade);
                events.Put(create);

                //var factory = new ProductFactory(connector);

                //(var trade, var evt) =
                //    factory.CreateOption(10, 100, "GOLDMAN.LDN", "OPTEUR", "AXA", 100, false, true, false, 6);

                //events.Put(evt);
                //trades.Put(trade);

                //int quantity = 500000;

                //var tid = connector.GenerateUniqueIds("trade_id", quantity);

                //var cid = connector.GenerateUniqueIds("contract_id", quantity);
                //List<Trade> swaps = new List<Trade>(quantity);
                //for (int i = 0; i < quantity; i++)
                //{
                //    var tr = newTrade.Clone();
                //    tr.Id = tid[i];
                //    tr.ContractId = "IRD-" + cid[i];
                //    swaps.Add(tr);
                //}

                var evts = EventGenerator(create, connector, 100).ToList();
                var watch = new Stopwatch();
                watch.Start();
                events.FeedMany(evts);
                watch.Stop();

                Console.WriteLine($"Inserting 100 events took {watch.ElapsedMilliseconds} ms");

                evts = EventGenerator(create, connector, 1000).ToList();
                watch = new Stopwatch();
                watch.Start();
                events.FeedMany(evts);
                watch.Stop();

                Console.WriteLine($"Inserting 1000 events took {watch.ElapsedMilliseconds} ms");


                evts = EventGenerator(create, connector, 10000).ToList();
                watch = new Stopwatch();
                watch.Start();
                events.FeedMany(evts);
                watch.Stop();

                Console.WriteLine($"Inserting 10000 events took {watch.ElapsedMilliseconds} ms");

                evts = EventGenerator(create, connector, 100000).ToList();
                watch = new Stopwatch();
                watch.Start();
                events.FeedMany(evts);
                watch.Stop();

                Console.WriteLine($"Inserting 100000 events took {watch.ElapsedMilliseconds} ms");

                var list = TradesGenerator(newTrade, connector, 100).ToList();
                watch = new Stopwatch();
                watch.Start();
                trades.FeedMany(list);
                watch.Stop();

                Console.WriteLine($"Inserting 100 swaps took {watch.ElapsedMilliseconds} ms");

                list = TradesGenerator(newTrade, connector, 1000).ToList();
                watch = new Stopwatch();
                watch.Start();
                trades.FeedMany(list);
                watch.Stop();

                Console.WriteLine($"Inserting 1000 swaps took {watch.ElapsedMilliseconds} ms");


                list = TradesGenerator(newTrade, connector, 10000).ToList();
                watch = new Stopwatch();
                watch.Start();
                trades.FeedMany(list);
                watch.Stop();

                Console.WriteLine($"Inserting 10000 swaps took {watch.ElapsedMilliseconds} ms");

                list = TradesGenerator(newTrade, connector, 20000).ToList();
                watch = new Stopwatch();
                watch.Start();
                trades.FeedMany(list);
                watch.Stop();

                Console.WriteLine($"Inserting 20000 swaps took {watch.ElapsedMilliseconds} ms");

                //var tradeReloaded = trades.Single(t => t.ContractId == trade.ContractId);
                //var eventReloaded = events.Single(e => e.DealId == trade.ContractId);


                //// apply an increase event
                //(var newVersion, var increase) =
                //    factory.IncreaseOption(trade, 50);

                //trades.Put(trade);
                //trades.Put(newVersion);
                //events.Put(increase);

                //var allVersions = trades.Where(t => t.ContractId == trade.ContractId).ToList()
                //    .OrderBy(t => t.Version).ToList();
            }
        }
    }
}