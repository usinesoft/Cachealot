﻿using System;

namespace TradeProcessing.Helpers
{
    public abstract class EventInfo
    {
        public DateTime? EventDate { get; set; }
        public DateTime? ValueDate { get; set; }
    }
}