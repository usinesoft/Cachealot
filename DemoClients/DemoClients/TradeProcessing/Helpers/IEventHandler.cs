﻿using Cachealot.Linq;
using TradeProcessing.BusinessModel.Core;
using TradeProcessing.BusinessModel.Events;
using TradeProcessing.BusinessModel.Instruments;

namespace TradeProcessing.Helpers
{
    public interface IEventHandler
    {
        /// <summary>
        /// Apply an event. If the trade is null the event is a creation
        /// A trade is allways returned
        /// </summary>
        /// <param name="eventInfo"></param>
        /// <param name="trade"></param>
        /// <returns></returns>
        (Trade trade, ProductEvent productEvent) ApplyEvent(EventInfo eventInfo, Trade trade = null);

        Connector Connector { get; set; }
    }
}