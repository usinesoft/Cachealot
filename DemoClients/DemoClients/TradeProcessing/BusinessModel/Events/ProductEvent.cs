﻿using System;
using CacheClient.Interface;


namespace TradeProcessing.BusinessModel.Events
{
    public abstract class ProductEvent
    {
        [PrimaryKey(KeyDataType.StringKey)] public int EventId { get; set; }

        [Index(KeyDataType.StringKey)] public abstract string EventType { get; }

        public string Comment { get; set; }

        /// <summary>
        /// The id of the deal after the event is applied. Seme events can create new versions of a deal (new id)
        /// </summary>
        [Index(KeyDataType.StringKey)] public string DealId { get; set; }


        [Index(KeyDataType.IntKey, true)] public DateTime EventDate { get; set; }

        [Index(KeyDataType.IntKey, true)] public DateTime ValueDate { get; set; }

        [Index(KeyDataType.IntKey, false)] public abstract bool IsNegotiatedEvent { get; }

        /// <summary>
        /// True if the event is creating the first version of a trade (like Create, StepIn)
        /// </summary>
        [Index(KeyDataType.IntKey, false)] public abstract bool IsCreateEvent { get; }

        public override string ToString()
        {
            return $"{nameof(EventId)}: {EventId}, {nameof(EventType)}: {EventType}";
        }
    }
}