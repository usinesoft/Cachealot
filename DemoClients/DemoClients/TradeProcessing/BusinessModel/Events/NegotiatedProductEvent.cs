﻿namespace TradeProcessing.BusinessModel.Events
{
    public abstract class NegotiatedProductEvent : ProductEvent
    {
        public abstract bool NeedsConfirmation { get; }

        public override bool IsNegotiatedEvent => true;


    }
}