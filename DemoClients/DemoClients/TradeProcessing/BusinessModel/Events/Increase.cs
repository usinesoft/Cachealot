﻿using System;

namespace TradeProcessing.BusinessModel.Events
{
    public class Increase : NegotiatedProductEvent
    {
        public Increase(int id, decimal delta, string dealId)
        {
            EventId = id;
            Delta = delta;

            DealId = dealId;

            EventDate = DateTime.Today;
            ValueDate = DateTime.Today;
        }

        public Increase()
        {
        }

        public decimal Delta { get; set; }

        public override bool NeedsConfirmation => true;

        public override string EventType => "INCREASE";

        public override bool IsCreateEvent => false;
    }
}