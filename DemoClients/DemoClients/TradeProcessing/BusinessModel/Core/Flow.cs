﻿using System;

namespace TradeProcessing.BusinessModel.Instruments
{
    public class Flow
    {
        public string Underlying { get; set; }

        public decimal Quantity { get; set; }

        public DateTime PaymentDate { get; set; }
    }
}