﻿namespace TradeProcessing.BusinessModel.Instruments
{
    public enum RateType
    {
        Fixed,
        Variable
    }
}