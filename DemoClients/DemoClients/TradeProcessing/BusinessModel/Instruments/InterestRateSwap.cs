﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeProcessing.BusinessModel.Instruments
{
    public class InterestRateSwap:IProduct
    {
        public string Name => "InterestRateSwap";
        public AssetClass AssetClass =>AssetClass.Ird;
        public DateTime? MaturityDate { get; set; }
        public decimal Quantity => 1;
        public decimal UnitPrice { get; set; }
        public decimal Notional { get; set; }

        public SwapLeg PayLeg { get;  } = new SwapLeg();

        public SwapLeg ReceiveLeg { get; } = new SwapLeg();
    }


}
