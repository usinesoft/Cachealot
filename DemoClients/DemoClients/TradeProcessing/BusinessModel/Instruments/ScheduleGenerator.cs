﻿using System;
using System.Collections.Generic;

namespace TradeProcessing.BusinessModel.Instruments
{
    public static class ScheduleGenerator
    {

        /// <summary>
        /// Very simple for now
        /// </summary>
        /// <returns></returns>
        public static DateTime AdjustDate(DateTime toAdjust)
        {
            if (toAdjust.DayOfWeek == DayOfWeek.Saturday)
            {
                return toAdjust.AddDays(2);
            }

            if (toAdjust.DayOfWeek == DayOfWeek.Sunday)
            {
                return toAdjust.AddDays(1);
            }

            return toAdjust;
        }

        public static IList<DateTime> GenerateSchedule(DateTime startDate, DateTime endDate, string tenor)
        {
            int frequency = 0; // days between flows

            // for now only tenors like 3M 6M
            if (tenor.ToUpper().EndsWith("M"))
            {
                var months = tenor.Substring(0, tenor.Length - 1);
                frequency = int.Parse(months) * 30;

            }
            List<DateTime> dates = new List<DateTime>();

            for (DateTime date = startDate; date <= endDate; date = date.AddDays(frequency))
            {
                var adjusted = AdjustDate(date);
                dates.Add(adjusted);
            }

            


            return dates;
        }
    }
}