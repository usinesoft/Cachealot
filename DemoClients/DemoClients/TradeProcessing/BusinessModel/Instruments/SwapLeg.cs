﻿using System.Collections.Generic;

namespace TradeProcessing.BusinessModel.Instruments
{
    public class SwapLeg
    {
        public RateType RateType { get; set; }

        public decimal ? FixedRate { get; set; }

        public string VariableRate { get; set; }

        public IList<Flow> Flows { get; set; } = new List<Flow>();
    }
}