﻿using System;

namespace TradeProcessing.Helpers
{
    public class CreateInterestRateSwapEventinfo : EventInfo
    {
        public DateTime MaturityDate { get; set; }

        public string Tenor { get; set; }

        public string Owner { get; set; }
        public string Counterparty { get; set; }
        public string Portfolio { get; set; }

        public string VariableRate { get; set; }

        public decimal FixedRate { get; set; }
        public decimal Notional { get; set; }

        
        
    }
}