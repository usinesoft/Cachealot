﻿using Cachealot.Linq;
using JetBrains.Annotations;
using System;
using TradeProcessing.BusinessModel.Core;
using TradeProcessing.BusinessModel.Events;
using TradeProcessing.BusinessModel.Instruments;
using TradeProcessing.Helpers;
using EventInfo = TradeProcessing.Helpers.EventInfo;

namespace TradeProcessing.BusinessModel.Handlers
{
    class CreateInterestRateSwapEventHandler : IEventHandler
    {
        public Connector Connector { get; set; }


        public (Trade trade, ProductEvent productEvent) ApplyEvent([NotNull] EventInfo eventInfo, Trade trade = null)
        {
            if (eventInfo == null) throw new ArgumentNullException(nameof(eventInfo));

            var tid = Connector.GenerateUniqueIds("trade_id", 1);

            var cid = Connector.GenerateUniqueIds("contract_id", 1);

            var eid = Connector.GenerateUniqueIds("event_id", 1);


            var info = (CreateInterestRateSwapEventinfo) eventInfo;


            var newTrade = new Trade
            {
                ContractId = "IRD-" + cid[0],
                Id = tid[0],
                ValueDate = DateTime.Today.AddDays(1),
                TradeDate = DateTime.Today,
                Timestamp = DateTime.Now,
                Counterparty = info.Counterparty,
                IsDestroyed = false,
                Portfolio = info.Portfolio,
                Version = 1,
                IsLastVersion = true
            };

            var swap = new InterestRateSwap
            {
                Notional = info.Notional,
                MaturityDate = info.MaturityDate
            };

            var dates = ScheduleGenerator.GenerateSchedule(newTrade.ValueDate, swap.MaturityDate.Value, info.Tenor);
            // generate the fixed leg (assume we pay the fixed rate for now)
            var fixedCoupon = info.FixedRate * info.Notional; //not the real calculation
            foreach (var date in dates)
            {
                swap.PayLeg.Flows.Add(new Flow{PaymentDate = date,Quantity = fixedCoupon, Underlying = "EUR"});
            }

            foreach (var date in dates)
            {
                swap.ReceiveLeg.Flows.Add(new Flow { PaymentDate = date, Underlying = "EUR" });
            }

            

            newTrade.Product = swap;

            var creationEvent  = new Create(eid[0], newTrade.ContractId);


            return (newTrade, creationEvent);
        }
    }
}