﻿using System;
using System.Reflection;
using TradeProcessing.Helpers;

namespace TradeProcessing.BusinessModel.Handlers
{
    public static class Handlers
    {

        /// <summary>
        /// Convention based. The class must be named ActionInstrumentEventHandler
        /// </summary>
        /// <param name="instrumentName"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static IEventHandler GetHandler(string instrumentName, string action)
        {
            var assembly =  Assembly.GetAssembly(typeof(IEventHandler));

            var handlerType = assembly.GetType("TradeProcessing.BusinessModel.Handlers." + action + instrumentName + "EventHandler", true, true);

            return (IEventHandler) Activator.CreateInstance(handlerType);
        }
    }
}