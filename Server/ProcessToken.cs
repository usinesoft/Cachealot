using System;
using System.Threading;

namespace Cachealot.Server
{
    /// <summary>
    /// Used to manage processes that run in paralel on multiple datastores
    /// </summary>
    public class ProcessToken
    {
        public ManualResetEvent Finished { get; } = new ManualResetEvent(false);

        public Exception Exception { get; set; }
    }
}