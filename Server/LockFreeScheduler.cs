#region

using System.Diagnostics;
using System.Threading;

#endregion

namespace Cachealot.Server
{
    /// <summary>
    ///     Works mostly like a producer/consumer with integrated access control
    ///     Read only access tasks are parallelized, write access ones are executed sequentially
    ///     Allows to execute parallel tasks which share common resources without the need of access control
    ///     on the resource itself
    /// </summary>
    /// <typeparam name="TInputData"></typeparam>
    public class LockFreeScheduler<TInputData> where TInputData : class
    {
        private readonly int _threadCount;
        private TaskQueues<TInputData> _taskQueues;

        private Thread[] _workers;

        public LockFreeScheduler(int threadCount)
        {
            _threadCount = threadCount;
        }

        /// <summary>
        ///     Start the scheduler
        /// </summary>
        public void Start()
        {
            _workers = new Thread[_threadCount];
            _taskQueues = new TaskQueues<TInputData>();

            for (int i = 0; i < _threadCount; i++)
            {
                _workers[i] = new Thread(() =>
                {
                    while (true)
                    {
                        var task = _taskQueues.WaitForTask();

                        Debug.Assert(task != null);

                        if (!task.IsStopMarker)
                        {
                            try
                            {
                                task.Run();
                            }
                            finally
                            {
                                if (task.NeedsWriteAccess)
                                    _taskQueues.EndWriteAccess();
                                else
                                    _taskQueues.EndReadOnlyAccess();
                            }
                        }
                        else //stop this working thread
                        {
                            break;
                        }
                    }
                }) {Name = "Lock free consumer"};
            }

            for (int i = 0; i < _threadCount; i++)
            {
                _workers[i].IsBackground = true;
                _workers[i].Start();
            }
        }

        /// <summary>
        ///     Stop the scheduler and wait for all threads to stop.
        ///     Blocks until all the pending workitems are processed
        /// </summary>
        public void Stop()
        {
            _taskQueues.Stop();

            foreach (var worker in _workers)
            {
                worker.Join();
            }
        }

        /// <summary>
        ///     The scheduler preserves the execution order and schedule tasks according to their access type
        ///     Read only access tasks are parallelized, write access ones are executed sequentially
        /// </summary>
        /// <param name="task"></param>
        public void ProcessTask(WorkItem<TInputData> task)
        {
            _taskQueues.NewTask(task);
        }
    }
}