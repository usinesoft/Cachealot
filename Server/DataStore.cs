#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Cachealot.Server.Persistence;
using CacheClient.ChannelInterface;
using CacheClient.Core;
using CacheClient.Interface;
using CacheClient.Messages;
using CacheClient.Profiling;
using CacheClient.Queries;
using CacheClient.Tools;

#endregion

namespace Cachealot.Server
{
    /// <summary>
    ///     A data store contains multiply indexed objects of the same type
    /// </summary>
    public class DataStore
    {
        /// <summary>
        ///     List of indexes for index keys (multiple objects by key value)
        /// </summary>
        private readonly Dictionary<string, IndexBase> _dataByIndexKey;

        /// <summary>
        ///     object by primary key
        /// </summary>
        private readonly Dictionary<KeyValue, CachedObject> _dataByPrimaryKey;

        /// <summary>
        ///     List of indexes for unique keys
        /// </summary>
        private readonly Dictionary<string, Dictionary<KeyValue, CachedObject>> _dataByUniqueKey;

        /// <summary>
        ///     Will contain the name of the index used for the last query or null if a fullscan was required
        /// </summary>
        private readonly ThreadLocal<ExecutionPlan> _lastExecutionPlan = new ThreadLocal<ExecutionPlan>();


        private readonly LockFreeScheduler<TaskInput> _scheduler;


        private long _count;

        /// <summary>
        ///     Description of data preloaded into the datastore
        /// </summary>
        private DomainDescription _domainDescription;

        private long _hitCount;


        private long _readCount;
        private long _updateCount;

        /// <summary>
        ///     Initialize an empty datastore from a type description
        /// </summary>
        /// <param name="typeDescription"></param>
        /// <param name="evictionPolicy"></param>
        public DataStore(TypeDescription typeDescription, EvictionPolicy evictionPolicy)
        {
            TypeDescription = typeDescription ?? throw new ArgumentNullException(nameof(typeDescription));

            EvictionPolicy = evictionPolicy ?? throw new ArgumentNullException(nameof(evictionPolicy));

            //initialize the primary key dictionary
            _dataByPrimaryKey = new Dictionary<KeyValue, CachedObject>();


            //initialize the unique keys dictionaries (une by unique key) 
            _dataByUniqueKey = new Dictionary<string, Dictionary<KeyValue, CachedObject>>();

            foreach (var keyInfo in typeDescription.UniqueKeyFields)
                _dataByUniqueKey.Add(keyInfo.Name, new Dictionary<KeyValue, CachedObject>());

            //initialize the indexes (one by index key)
            _dataByIndexKey = new Dictionary<string, IndexBase>();

            // scalar indexed fields
            foreach (var indexField in typeDescription.IndexFields)
            {
                var index = IndexFactory.CreateIndex(indexField);
                _dataByIndexKey.Add(indexField.Name, index);
            }


            // list indexed fields

            foreach (var indexField in typeDescription.ListFields)
            {
                var index = IndexFactory.CreateIndex(indexField);
                _dataByIndexKey.Add(indexField.Name, index);
            }

            //TODO read the number of threads from a configuration file
            _scheduler = new LockFreeScheduler<TaskInput>(4);
        }

        public void StartProcessingClientRequests()
        {
            Dbg.Trace($"Starting lock free scheduler for {TypeDescription.TypeName}");

            _scheduler.Start();
        }


        public ExecutionPlan LastExecutionPlan
        {
            get => _lastExecutionPlan.Value;
            private set => _lastExecutionPlan.Value = value;
        }

        public TypeDescription TypeDescription { get; }

        public EvictionType EvictionType => EvictionPolicy.Type;

        public long Count => Interlocked.Read(ref _count);

        public EvictionPolicy EvictionPolicy { get; }

        public Profiler Profiler { private get; set; }

        /// <summary>
        ///     Description of data preloaded into the datastore
        /// </summary>
        public DomainDescription DomainDescription
        {
            get => _domainDescription;
            private set => _domainDescription = value;
        }


        public long HitCount => Interlocked.Read(ref _hitCount);

        public long ReadCount => Interlocked.Read(ref _readCount);
        public PersistenceEngine PersistenceEngine { get; set; }

        /// <summary>
        ///     object by primary key
        /// </summary>
        public Dictionary<KeyValue, CachedObject> DataByPrimaryKey => _dataByPrimaryKey;


        /// <summary>
        ///     Store a new object in all the indexes
        ///     REQUIRE: an object with the same primary key is not present in the datastore
        /// </summary>
        /// <param name="cachedObject"></param>
        /// <param name="excludeFromEviction">if true the item will never be evicted</param>
        internal void InternalAddNew(CachedObject cachedObject, bool excludeFromEviction)
        {
            InternalAddNew(cachedObject);

            Interlocked.Increment(ref _count);

            if (!excludeFromEviction)
                EvictionPolicy.AddItem(cachedObject);

            if (EvictionPolicy.IsEvictionRequired)
            {
                //if eviction is required the server posts itself an EvictionRequest which will
                //be executed later by the LockFreeScheduler
                var toRemove = EvictionPolicy.DoEviction();
                ProcessRequest(new EvictionRequest(TypeDescription.FullTypeName, toRemove), null);
            }
        }


        public void Dump(DumpRequest request, int shardIndex, ProcessToken token)
        {
            var requestWorkitem = new WorkItem<TaskInput>(false,
                new TaskInput(null, request),
                input =>
                {
                    try
                    {
                        var dumpRequest = (DumpRequest) input.Request;
                        InternalDump(dumpRequest.Path, shardIndex);
                    }
                    catch (Exception e)
                    {
                        token.Exception = e;
                    }
                    finally
                    {
                        token.Finished.Set();
                    }
                });


            _scheduler.ProcessTask(requestWorkitem);
        }

        private void InternalAddNew(CachedObject cachedObject)
        {
            if (cachedObject.FullTypeName != TypeDescription.FullTypeName)
                throw new InvalidOperationException(
                    $"An object of type {cachedObject.FullTypeName} can not be stored in DataStore of type {TypeDescription.FullTypeName}");


            var primaryKey = cachedObject.PrimaryKey;
            if (ReferenceEquals(primaryKey, null))
                throw new InvalidOperationException("can not store an object having a null primary key");


            _dataByPrimaryKey.Add(primaryKey, cachedObject);


            if (cachedObject.UniqueKeys != null)
                foreach (var uniqueKey in cachedObject.UniqueKeys)
                {
                    var dictionaryToUse = _dataByUniqueKey[uniqueKey.KeyName];
                    dictionaryToUse.Add(uniqueKey, cachedObject);
                }


            foreach (var index in _dataByIndexKey)
                index.Value.Put(cachedObject);
        }


        internal void LoadFromDump(string path, int shardIndex)
        {
            foreach (var cachedObject in DumpHelper.ObjectsInDump(path, TypeDescription, shardIndex))
            {
                InternalAddNew(cachedObject);

                // only in debug, only if this simulation was activated (for tests only)
                Dbg.SimulateException(100, shardIndex);
            }
        }

        private void InternalDump(string path, int shardIndex)
        {
            DumpHelper.DumpObjects(path, TypeDescription, shardIndex, _dataByPrimaryKey.Values);
        }

        /// <summary>
        ///     Remove the object from all indexes
        /// </summary>
        /// <param name="primary"></param>
        private CachedObject InternalRemoveByPrimaryKey(KeyValue primary)
        {
            Dbg.Trace($"remove by primary key {primary}");

            if (primary.KeyName != TypeDescription.PrimaryKeyField.Name)
                throw new NotSupportedException($"{primary.KeyName} is not a primary key");

            if (!_dataByPrimaryKey.ContainsKey(primary))
                return null;

            var toRemove = _dataByPrimaryKey[primary];
            _dataByPrimaryKey.Remove(primary);


            if (toRemove.UniqueKeys != null)
                foreach (var uniqueKey in toRemove.UniqueKeys)
                    _dataByUniqueKey[uniqueKey.KeyName].Remove(uniqueKey);


            foreach (var index in _dataByIndexKey)
                index.Value.RemoveOne(toRemove);


            return toRemove;
        }


        internal void InternalTruncate()
        {
            EvictionPolicy.Clear();
            _dataByPrimaryKey.Clear();

            foreach (var indexByKey in _dataByUniqueKey)
                indexByKey.Value.Clear();

            foreach (var index in _dataByIndexKey)
                index.Value.Clear();


            // TODO interlocked not required because a lock free scheduler is used
            Interlocked.Exchange(ref _updateCount, 0);
            Interlocked.Exchange(ref _readCount, 0);
            Interlocked.Exchange(ref _hitCount, 0);
            Interlocked.Exchange(ref _count, 0);

            // also reset the domain description
            Interlocked.Exchange(ref _domainDescription, null);
        }

        public void RemoveByPrimaryKey(KeyValue primary)
        {
            var removed = InternalRemoveByPrimaryKey(primary);
            if (removed != null)
            {
                EvictionPolicy.TryRemove(removed);

                Interlocked.Decrement(ref _count);
            }
        }

        /// <summary>
        ///     Update an object previously stored
        ///     The primary key must be the same, all others can change
        /// </summary>
        /// <param name="item"></param>
        private void InternalUpdate(CachedObject item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            if (!_dataByPrimaryKey.ContainsKey(item.PrimaryKey))
            {
                var msg = $"Update called for the object {item} which is not stored in the cache";
                throw new NotSupportedException(msg);
            }

            InternalRemoveByPrimaryKey(item.PrimaryKey);
            InternalAddNew(item);

            EvictionPolicy.Touch(item);
        }


        /// <summary>
        ///     Get unique object by primary or unique key
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        internal CachedObject InternalGetOne(KeyValue keyValue)
        {
            CachedObject result = null;

            if (keyValue == (KeyValue) null)
                throw new ArgumentNullException(nameof(keyValue));

            if (keyValue.KeyType == KeyType.Primary)
                if (_dataByPrimaryKey.ContainsKey(keyValue))
                {
                    result = _dataByPrimaryKey[keyValue];
                    EvictionPolicy.Touch(result);
                }

            if (keyValue.KeyType == KeyType.Unique)
                if (_dataByUniqueKey.ContainsKey(keyValue.KeyName))
                    if (_dataByUniqueKey[keyValue.KeyName].ContainsKey(keyValue))
                    {
                        result = _dataByUniqueKey[keyValue.KeyName][keyValue];
                        EvictionPolicy.Touch(result);
                    }


            if (keyValue.KeyType == KeyType.Unique || keyValue.KeyType == KeyType.Primary) return result;


            throw new NotSupportedException(
                $"GetOne() called with the key {keyValue.KeyName} which is neither primary nor unique");
        }

        /// <summary>
        ///     Ges a subset by index key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal IList<CachedObject> InternalGetMany(KeyValue key)
        {
            if (key == (KeyValue) null)
                throw new ArgumentNullException(nameof(key));

            if (key.KeyType != KeyType.ScalarIndex)
                throw new ArgumentException("GetMany() called with a non index key", nameof(key));

            if (!_dataByIndexKey.ContainsKey(key.KeyName))
                throw new NotSupportedException($"GetMany() called with the unknown index key {key} ");

            return _dataByIndexKey[key.KeyName].GetMany(new List<KeyValue> {key}).ToList();
        }


        internal IList<CachedObject> InternalGetMany(KeyValue keyValue, QueryOperator op)
        {
            return InternalFind(new AtomicQuery(keyValue, op));
        }


        /// <summary>
        ///     Get a subset by index key value and comparison operator (atomic query)
        ///     Atomic queries re resolved by a single index
        /// </summary>
        /// <returns></returns>
        private IList<CachedObject> InternalFind(AtomicQuery atomicQuery, int count = 0)
        {
            if (!atomicQuery.IsValid)
                throw new NotSupportedException("Invalid atomic query: " + atomicQuery);

            var indexName = atomicQuery.IndexName;

            //////////////////////////////////////////////////////////////////////////
            // 3 cases: In + multiple values, Btw + 2 values, OTHER + 1 value


            if (atomicQuery.Operator == QueryOperator.In)
            {
                var result = new Dictionary<KeyValue, CachedObject>();

                foreach (var value in atomicQuery.InValues)
                {
                    if (count > 0 && result.Count >= count) break;

                    if (_dataByIndexKey.ContainsKey(value.KeyName))
                    {
                        var byOneValue = _dataByIndexKey[value.KeyName].GetMany(new List<KeyValue> {value});

                        foreach (var cachedObject in byOneValue)
                            result[cachedObject.PrimaryKey] = cachedObject; // overrite duplicates
                    }
                    else if (_dataByUniqueKey.ContainsKey(value.KeyName))
                    {
                        if (_dataByUniqueKey[value.KeyName].TryGetValue(value, out var cachedObject))
                            result[cachedObject.PrimaryKey] = cachedObject;
                    }
                    else if (TypeDescription.PrimaryKeyField.Name == value.KeyName)
                    {
                        if (_dataByPrimaryKey.TryGetValue(value, out var cachedObject))
                            result[cachedObject.PrimaryKey] = cachedObject;
                    }
                }


                LastExecutionPlan = new ExecutionPlan {PrimaryIndexName = indexName};

                return result.Values.ToList();
            }

            var index = _dataByIndexKey[indexName];
            var canBeProcessedByIndex = !(atomicQuery.Operator == QueryOperator.In && index.IsOrdered);

            if (atomicQuery.Operator != QueryOperator.Eq && atomicQuery.Operator != QueryOperator.In &&
                !index.IsOrdered)
                canBeProcessedByIndex = false;

            // if indexes can not be used proceed to full scan
            if (!canBeProcessedByIndex)
            {
                LastExecutionPlan = new ExecutionPlan(); // empty execution plan means full scan
                return _dataByPrimaryKey.Values.Where(atomicQuery.Match).ToList();
            }

            // Btw and comparison operators are managed by a single call to the index
            return _dataByIndexKey[indexName].GetMany(atomicQuery.Values, atomicQuery.Operator).ToList();
        }


        /// <summary>
        ///     Return a subset matching an OrQuery (sum of <see cref="AndQuery" /> (product of <see cref="AtomicQuery" />))
        ///     Will be decomposed as union of results of AND queries
        /// </summary>
        /// <param name="query"></param>
        /// <param name="onlyIfComplete"> </param>
        /// <returns></returns>
        internal IList<CachedObject> InternalGetMany(OrQuery query, bool onlyIfComplete = false)
        {
            var result = InternalFind(query, onlyIfComplete);

            EvictionPolicy.Touch(result);

            return result;
        }


        private IList<CachedObject> InternalFind(OrQuery query, bool onlyIfComplete = false)
        {
            Dbg.Trace($"begin InternalFind with query {query}");

            var dataIsComplete = _domainDescription != null && _domainDescription.IsFullyLoaded;

            if (onlyIfComplete && !dataIsComplete)
                throw new CacheException("Full data is not available for type " + TypeDescription.FullTypeName);

            var result = new List<CachedObject>();

            // if empty query return all
            if (query.Elements.Count == 0)
            {
                Dbg.Trace($"InternalFind with empty query: retun all {query.TypeName}");

                result.AddRange(query.Take > 0 ? _dataByPrimaryKey.Values.Take(query.Take) : _dataByPrimaryKey.Values);

                return result;
            }

            var take = query.Take;

            var remaining = take;
            foreach (var andQuery in query.Elements)
                // 0 means no limit
                if (take > 0)
                {
                    result.AddRange(InternalFind(andQuery, remaining));
                    if (result.Count < remaining)
                        remaining = take - result.Count;
                    else
                        break;
                }
                else
                {
                    result.AddRange(InternalFind(andQuery));
                }


            Dbg.Trace($"end InternalFind returned {result.Count} ");

            return result;
        }

        /// <summary>
        ///     Remove a subset of items specified by a query
        /// </summary>
        /// <param name="query"></param>
        /// <returns>number of items effectively removed</returns>
        private int RemoveMany(OrQuery query)
        {
            var result = 0;
            var toRemove = InternalFind(query);
            foreach (var cachedObject in toRemove)
            {
                if (InternalRemoveByPrimaryKey(cachedObject.PrimaryKey) != null)
                    result++;
                Interlocked.Decrement(ref _count);

                EvictionPolicy.TryRemove(cachedObject);
            }

            return result;
        }


        /// <summary>
        ///     Count the items matching the specified query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private int InternalEval(OrQuery query)
        {
            // for an empty query return the total number of elements
            if (query.Elements.Count == 0) return _dataByPrimaryKey.Count;

            var evalResult = query.Elements.Sum(InternalEval);

            // Take extension can be combined with Count extension
            return query.Take > 0 ? Math.Min(evalResult, query.Take) : evalResult;
        }

        /// <summary>
        ///     Count the items matched by an <see cref="AndQuery" />
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private int InternalEval(AndQuery query)
        {
            return InternalFind(query).Count;
        }


        /// <summary>
        ///     Returns a subset matching all the atomic queries of an <see cref="AndQuery" />
        /// </summary>
        /// <param name="query"></param>
        /// <param name="count">If > 0 return only the first count elements</param>
        /// <returns></returns>
        private IList<CachedObject> InternalFind(AndQuery query, int count = 0)
        {
            // void queries are illegal
            if (query.Elements.Count == 0)
                throw new NotSupportedException("Can not process an empty query");


            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Simplified processing for atomic queries(no need to choose which index to use first, because only one is involved)

            if (query.Elements.Count == 1)
            {
                var atomicQuery = query.Elements[0];

                // multiple result query
                if (atomicQuery.IndexType == KeyType.ScalarIndex || atomicQuery.IndexType == KeyType.ListIndex ||
                    atomicQuery.Operator == QueryOperator.In)
                {
                    if (count > 0) return InternalFind(atomicQuery).Take(count).ToList();
                    return InternalFind(atomicQuery);
                }

                // single result query
                var item = InternalGetOne(atomicQuery.Value);

                if (item != null)
                    return new List<CachedObject> {item};

                return new List<CachedObject>();
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Simple query optimizer
            // 1) Count the elements matched by each atomic query (indexes are optimized for counting: much faster than retrieving the elements)
            // 2) Use the one (the coresponding index) matching the minimum number of items as main query. This will produce a first subset of items (using the index)
            // 3) Iterate over the subset and eliminate the elements that do not match the rest of the queries

            var minimumItemsMatched = int.MaxValue;
            IndexBase primaryIndex = null;
            AtomicQuery primaryQuery = null;

            var foundWithUniqueKeys = new List<CachedObject>();

            foreach (var atomicQuery in query.Elements)
            {
                if (atomicQuery.Value?.KeyType == KeyType.Primary)
                {
                    if (_dataByPrimaryKey.TryGetValue(atomicQuery.Value, out var val))
                    {
                        foundWithUniqueKeys.Add(val);
                        continue;
                    }

                    // if the search by primary key failed no need to continue
                    return new CachedObject[0];
                }

                if (atomicQuery.Value?.KeyType == KeyType.Unique)
                {
                    if (_dataByUniqueKey[atomicQuery.Value.KeyName].TryGetValue(atomicQuery.Value, out var val))
                    {
                        foundWithUniqueKeys.Add(val);
                        continue;
                    }

                    // if the search by unique key failed no need to continue
                    return new CachedObject[0];
                }

                if (!_dataByIndexKey.ContainsKey(atomicQuery.IndexName))
                    throw new NotSupportedException(atomicQuery.IndexName + " is not an index key");

                var curIndex = _dataByIndexKey[atomicQuery.IndexName];
                var curItemsCount = curIndex.GetCount(atomicQuery.Values, atomicQuery.Operator);

                if (curItemsCount < minimumItemsMatched)
                {
                    minimumItemsMatched = curItemsCount;
                    primaryIndex = curIndex;
                    primaryQuery = atomicQuery;
                }
            }

            // Get a primary set directly using the index

            IEnumerable<CachedObject> primarySet;

            // in an efficient index was identified use it, otherwise do a fullscan
            if (primaryIndex != null)
            {
                primarySet = primaryIndex.GetMany(primaryQuery.Values, primaryQuery.Operator);
                LastExecutionPlan = new ExecutionPlan
                {
                    PrimaryIndexName = primaryIndex.Name,
                    ElementsInPrimarySet = minimumItemsMatched
                };
            }
            else
            {
                primarySet = _dataByPrimaryKey.Values;
                // empty execution plan means a full scan was required
                LastExecutionPlan = new ExecutionPlan();
            }

            primarySet = primarySet.Union(foundWithUniqueKeys);

            // Match all the items in the primary set against the query

            // if an index was used remove the test that was already matched by the index
            var simplifiedQuery = query.Clone();

            if (primaryQuery != null)
                simplifiedQuery.Elements.Remove(primaryQuery);

            if (count > 0) return primarySet.Where(simplifiedQuery.Match).Take(count).ToList();
            return primarySet.Where(simplifiedQuery.Match).ToList();
        }

        /// <summary>
        ///     This method is called from multiple threads. It creates and posts tasks to the lock-free
        ///     scheduler
        /// </summary>
        /// <param name="dataRequest"></param>
        /// <param name="client"></param>
        public void ProcessRequest(DataRequest dataRequest, IClient client)
        {
            var requestWorkitem = new WorkItem<TaskInput>(dataRequest.AccessType == DataAccessType.Write,
                new TaskInput(client, dataRequest),
                input =>
                    InternalProcessRequest((DataRequest) input.Request, input.Client));

            _scheduler.ProcessTask(requestWorkitem);
        }

        /// <summary>
        /// </summary>
        /// <param name="items"></param>
        /// <param name="persist">if false it is called internally while loading data so no need to rewrite it to storage</param>
        /// <param name="excludeFromEviction"></param>
        internal void InternalPutMany(IList<CachedObject> items, bool persist = true, bool excludeFromEviction = true)
        {

            var isBulkOperation = items.Count > Constants.BulkThreshold;

            List<CachedObject> toUpdate = new List<CachedObject>();

            try
            {
                Dbg.Trace($"begin InternalPutMany with {items.Count} object");
                

                if (persist) PersistenceEngine?.NewTransaction(new PutTransaction {Items = items});


                InternalBeginBulkInsert(isBulkOperation);

                foreach (var item in items)
                    
                    if (_dataByPrimaryKey.ContainsKey(item.PrimaryKey))
                    {
                        toUpdate.Add(item);
                        
                    }
                    else
                    {
                        InternalAddNew(item, excludeFromEviction);
                    }
            }
            finally
            {
                InternalEndBulkInsert(isBulkOperation);

                // update items outside the bulk insert
                foreach (var cachedObject in toUpdate)
                {
                    InternalUpdate(cachedObject);
                    Interlocked.Increment(ref _updateCount);
                }

                Dbg.Trace($"end InternalPutMany with {items.Count} object");
            }
        }


        private readonly Dictionary<string, List<CachedObject>> _feedSessions =
            new Dictionary<string, List<CachedObject>>();

        private void InternalProcessRequest(DataRequest dataRequest, IClient client)
        {
            var requestDescription = "";
            var processedItems = 0;
            var requestType = "UNKNOWN";


            try
            {
                Profiler.Start("data request");
                Response toSend = null;
                if (dataRequest.AccessType == DataAccessType.Write)
                {
                    if (dataRequest is PutRequest put)
                    {
                        if (put.SessionId != null)
                        {
                            if (!_feedSessions.TryGetValue(put.SessionId, out var alreadyReceived))
                            {
                                alreadyReceived = new List<CachedObject>();
                                _feedSessions[put.SessionId] = alreadyReceived;
                            }

                            alreadyReceived.AddRange(put.Items);

                            if (put.EndOfSession)
                            {
                                // if all the packets have been received put everything at once
                                
                                _feedSessions.Remove(put.SessionId);

                                InternalPutMany(alreadyReceived, true, put.ExcludeFromEviction);
                            }
                        }
                        else
                        {
                            InternalPutMany(put.Items, true, put.ExcludeFromEviction);
                        }

                        if (put.Items.Count > 1)
                            requestDescription = $"put {put.Items.Count} items";
                        else if (put.Items.Count == 1)
                            requestDescription = put.Items[0].ToString();

                        processedItems = put.Items.Count;
                        requestType = "PUT";
                    }
                    else if (dataRequest is RemoveRequest removeRequest1)
                    {
                        var primaryKeyToRemove = removeRequest1.PrimaryKey;

                        PersistenceEngine?.NewTransaction(new DeleteTransaction {PrimaryKeys = {primaryKeyToRemove}});

                        RemoveByPrimaryKey(primaryKeyToRemove);

                        requestDescription = $"{removeRequest1.PrimaryKey.KeyName} = {removeRequest1.PrimaryKey}";
                        processedItems = 1;
                        requestType = "REMOVE";
                    }

                    else if (dataRequest is RemoveManyRequest removeRequest)
                    {
                        //a void query implies a TRUNCATE operation
                        //A TRUNCATE operation also resets the hit ratio

                        if (removeRequest.Query.Elements == null || removeRequest.Query.Elements.Count == 0)
                        {
                            PersistenceEngine?.NewTransaction(new DeleteTransaction
                            {
                                PrimaryKeys = _dataByPrimaryKey.Keys.ToList()
                            });

                            var items = _dataByUniqueKey.Count;
                            InternalTruncate();
                            requestDescription = string.Format(TypeDescription.TypeName.ToUpper());
                            processedItems = items;
                            requestType = "TRUNCATE";

                            toSend = new ItemsCountResponse(items);
                        }
                        else
                        {
                            if (PersistenceEngine != null)
                            {
                                var items = InternalFind(removeRequest.Query);
                                PersistenceEngine.NewTransaction(new DeleteTransaction
                                {
                                    PrimaryKeys = items.Select(i => i.PrimaryKey).ToList()
                                });
                            }

                            var removedItems = RemoveMany(removeRequest.Query);

                            requestDescription = $"{removeRequest.Query}";
                            processedItems = removedItems;
                            requestType = "REMOVE";

                            toSend = new ItemsCountResponse(removedItems);
                        }
                    }

                    else if (dataRequest is EvictionRequest evictionRequest)
                    {
                        var toRemove = evictionRequest.ItemsToRemove;
                        foreach (var item in toRemove)
                        {
                            InternalRemoveByPrimaryKey(item.PrimaryKey);
                            Interlocked.Decrement(ref _count);
                        }

                        requestDescription = string.Empty;

                        processedItems = evictionRequest.ItemsToRemove.Count;
                        requestType = "EVICTION";
                    }
                    else if (dataRequest is DomainDeclarationRequest domainRequest)
                    {
                        if (domainRequest.Action == DomainDeclarationAction.Set)
                            DomainDescription = domainRequest.Description;
                        else if (domainRequest.Action == DomainDeclarationAction.Add)
                            DomainDescription.Add(domainRequest.Description);
                        else if (domainRequest.Action == DomainDeclarationAction.Remove)
                            DomainDescription.Remove(domainRequest.Description);
                    }


                    //for internal requests (EvictionRequest) the client is not assigned
                    if (client != null)
                    {
                        if (toSend == null)
                            toSend = new NullResponse();

                        client.SendResponse(toSend);
                    }
                }
                else
                {
                    if (dataRequest is GetRequest getRequest1)
                    {
                        var query = getRequest1.Query;
                        var result = InternalGetMany(query, getRequest1.OnlyIfComplete);


                        requestDescription = "\n       --> " + query;
                        processedItems = result.Count;
                        requestType = "GET";

                        //we don't know how many items were expected so consider it as an atomic operation
                        //for the read counter

                        Interlocked.Increment(ref _readCount);

                        if (result.Count >= 1)
                            Interlocked.Increment(ref _hitCount);

                        client.SendMany(result);
                    }
                    else
                    {
                        if (dataRequest is GetDescriptionRequest getRequest)
                        {
                            var query = getRequest.Query;
                            var result = InternalGetMany(query);


                            requestDescription = query.ToString();
                            processedItems = result.Count;
                            requestType = "SELECT";


                            client.SendManyGeneric(result);
                        }
                        else
                        {
                            if (dataRequest is EvalRequest request1)
                            {
                                var query = request1.Query;
                                var count = InternalEval(query);

                                var completeDataAvailable = false;

                                if (_domainDescription != null)
                                    completeDataAvailable = query.IsSubsetOf(_domainDescription);

                                requestDescription = query.ToString();
                                processedItems = count;
                                requestType = "EVAL";

                                var response = new EvalResponse {Items = count, Complete = completeDataAvailable};
                                client.SendResponse(response);
                            }

                            else
                            {
                                if (dataRequest is GetAvailableRequest request)
                                {
                                    var missingObjects = new List<KeyValue>();
                                    var foundObjects = new List<CachedObject>();

                                    InternalGetAvailableObjects(request.PrimaryKeys, request.MoreCriteria,
                                        missingObjects,
                                        foundObjects);

                                    foreach (var _ in missingObjects)
                                        Interlocked.Increment(ref _readCount);
                                    foreach (var _ in foundObjects)
                                    {
                                        Interlocked.Increment(ref _readCount);
                                        Interlocked.Increment(ref _hitCount);
                                    }

                                    requestDescription = "GET AVAILABLE: " + request.PrimaryKeys.Count;
                                    processedItems = foundObjects.Count;
                                    requestType = "GET MANY";

                                    client.SendMany(missingObjects, foundObjects);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                client?.SendResponse(new ExceptionResponse(ex));
            }
            finally
            {
                var data = Profiler.End();

                if (LastExecutionPlan != null && dataRequest is GetRequest)
                    requestDescription += "[ plan=" + LastExecutionPlan + "]";
                LastExecutionPlan = null;

                ServerLog.AddEntry(new ServerLogEntry(data.TotalTimeMiliseconds, requestType, requestDescription,
                    processedItems));
            }
        }

        
        private void InternalEndBulkInsert(bool transactional)
        {
            if (!transactional)
                return;

            Parallel.ForEach(_dataByIndexKey.Where(p => p.Value is OrderedIndex), pair => { pair.Value.EndFill(); });
         
        }

        private void InternalBeginBulkInsert(bool transactional)
        {
            if (!transactional)
                return;

            foreach (var index in _dataByIndexKey)
                index.Value.BeginFill();
        }


        /// <summary>
        ///     Get all available objects and return the missing ones
        ///     Each element from <see cref="keyValues" /> plus <see cref="moreCriteria" /> should match at most one object
        ///     If <see cref="moreCriteria" /> is null, then the keyValues should be unique ones
        /// </summary>
        /// <param name="keyValues"></param>
        /// <param name="moreCriteria"></param>
        /// <param name="missingObjects"></param>
        /// <param name="foundObjects"></param>
        private void InternalGetAvailableObjects(IEnumerable<KeyValue> keyValues, Query moreCriteria,
            ICollection<KeyValue> missingObjects, IList<CachedObject> foundObjects)
        {
            if (moreCriteria == null) //optimize search for unique keys
                foreach (var keyValue in keyValues)
                {
                    if (keyValue.KeyType != KeyType.Primary && keyValue.KeyType != KeyType.Unique)
                        throw new NotSupportedException("Not an unique key : " + keyValue.KeyName);

                    var found = InternalGetOne(keyValue);
                    if (found != null)
                        foundObjects.Add(found);
                    else
                        missingObjects.Add(keyValue);
                }
            else
                foreach (var keyValue in keyValues)
                {
                    IList<CachedObject> objects;

                    if (keyValue.KeyType == KeyType.ScalarIndex)
                    {
                        objects = InternalGetMany(keyValue);
                    }
                    else
                    {
                        var obj = InternalGetOne(keyValue);
                        objects = new List<CachedObject>();
                        if (obj != null)
                            objects.Add(obj);
                    }

                    var matchingObject = objects.FirstOrDefault(moreCriteria.Match);

                    if (matchingObject != null)
                        foundObjects.Add(matchingObject);
                    else
                        missingObjects.Add(keyValue);
                }

            EvictionPolicy.Touch(foundObjects);
        }

        public void Stop()
        {
            _scheduler.Stop();
        }

        private class TaskInput
        {
            public TaskInput(IClient client, Request request)
            {
                Client = client;
                Request = request;
            }

            public IClient Client { get; }

            public Request Request { get; }
        }
    }
}