using System;
using CacheClient.Messages;

namespace Cachealot.Server
{
    public static class IndexFactory
    {
        public static IndexBase CreateIndex(KeyInfo keyInfo)
        {
            if (keyInfo == null)
                throw new ArgumentNullException("keyInfo");

            if (keyInfo.IsOrdered)
                return new OrderedIndex(keyInfo);
            return new DictionaryIndex(keyInfo);
        }
    }
}