using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Cachealot.Server.Persistence;
using CacheClient.ChannelInterface;
using CacheClient.Core;
using CacheClient.Messages;
using CacheClient.Profiling;
using CacheClient.Tools;
using Topshelf;

namespace Cachealot.Server
{
    public class Server
    {
        private readonly string _workingDirectory;

        private enum ServerMode
        {
            Normal,
            DuringDumpImport,
            ReadOnly
        }


        private ServerMode Mode { get; set; }

        private bool IsPersistent { get; }

        private DataContainer _dataContainer;

        /// <summary>
        /// signaled when all the loaders have finished loading data
        /// </summary>
        private readonly ManualResetEvent _eventAllLoadersFinished = new ManualResetEvent(false);

        private readonly Profiler _serverProfiler = new Profiler();

        private IServerChannel _channel;


        private DateTime _startTime;
        private PersistenceEngine _persistenceEngine;

        public Server(ServerConfig config, bool isPersistent = false, string workingDirectory = null)
        {
            _workingDirectory = workingDirectory;
            IsPersistent = isPersistent;
            _dataContainer = new DataContainer(config, _serverProfiler);
            
        }

        public IServerChannel Channel
        {
            private get => _channel;
            set
            {
                if (_channel != null)
                    _channel.RequestReceived -= HandleRequestReceived;

                _channel = value;

                _channel.RequestReceived += HandleRequestReceived;
            }
        }

        public HostControl HostControl { get; set; }


        /// <summary>
        /// This call comes from different threads
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleRequestReceived(object sender, RequestEventArgs e)
        {


            // a very short lock. All data requests are processed asynchronously
            lock (_dataContainer)
            {
                if (e.Request is ImportDumpRequest importRequest)
                {
                    ManageImportRequest(importRequest, e.Client);
                }
                else if (e.Request is StopRequest stopRequest)
                {
                    if (stopRequest.Restart)
                    {
                        ServerLog.LogWarning("restart request received");
                        HostControl?.Restart();
                    }
                    else
                    {
                        ServerLog.LogWarning("stop request received");
                        HostControl?.Stop();
                    }
                    
                }
                else if(e.Request is SwitchModeRequest switchModeRequest)
                {
                    Mode = switchModeRequest.NewMode == 1 ? ServerMode.ReadOnly : ServerMode.Normal;
                    e.Client.SendResponse(new NullResponse());
                }
                else if (e.Request is DropRequest)
                {
                    try
                    {
                        ServerLog.LogWarning("drop request received");
                        ManageDropRequest();
                        ServerLog.LogWarning("all data was deleted");
                        e.Client.SendResponse(new NullResponse());
                    }
                    catch (Exception exception)
                    {
                        e.Client.SendResponse(new ExceptionResponse(exception));
                    }
                }
                else
                {
                    if (Mode == ServerMode.DuringDumpImport)
                    {
                        e.Client.SendResponse(new ExceptionResponse(
                            new NotSupportedException("Database is not available while restoring data from dump")));
                        return;
                    }

                    if (Mode == ServerMode.ReadOnly && e.Request is DataRequest dataRequest && dataRequest.AccessType == DataAccessType.Write)
                    {
                        e.Client.SendResponse(new ExceptionResponse(
                            new NotSupportedException("Database is in read-only mode")));
                        return;
                    }

                    //as the data container does not have access to the channel
                    //let it know how many connections are active 
                    long activeConnections = Channel.Connections;
                    _dataContainer.ActiveConnections = activeConnections;
                    _dataContainer.StartTime = _startTime;


                    // this call will be processed asynchronously
                    _dataContainer.DispatchRequest(e.Request, e.Client);
                }
            }
        }


        private void ManageDropRequest()
        {
            Mode = ServerMode.ReadOnly;
            _persistenceEngine.Stop();
            
            // delete persistent data
            _persistenceEngine.StoreDataForRollback();

            // delete data from memory
            _dataContainer = new DataContainer(new ServerConfig(), _serverProfiler);
            _persistenceEngine.Container = _dataContainer;
            _dataContainer.PersistenceEngine = _persistenceEngine;

            _persistenceEngine.LightStart();

            foreach (var dataStore in _dataContainer.DataStores.Values)
            {
                dataStore.StartProcessingClientRequests();
            }

            _persistenceEngine.DeleteRollbackData();
            Mode = ServerMode.Normal;
        }

        private void ManageImportRequest(ImportDumpRequest importRequest, IClient client)
        {
            try
            {
                ServerLog.LogInfo($"begin import dump stage {importRequest.Stage} on shard {importRequest.ShardIndex}");

                switch (importRequest.Stage)
                {
                    case 0:

                        Mode = ServerMode.DuringDumpImport;
                        _persistenceEngine.Stop();
                        _persistenceEngine.StoreDataForRollback();
                        break;

                    case 1:

                        using (var storage = new ReliableStorage(new NullObjectProcessor(), _workingDirectory))
                        {
                            // first copy the schema and reinitialize data stores
                            string path = DumpHelper.NormalizeDumpPath(importRequest.Path);

                            var dataPath = _workingDirectory != null
                                ? Path.Combine(_workingDirectory, Constants.DataPath)
                                : Constants.DataPath;
                            File.Copy(Path.Combine(path, Constants.SchemaFileName),
                                Path.Combine(dataPath, Constants.SchemaFileName));

                            _dataContainer = new DataContainer(new ServerConfig(), _serverProfiler);
                            _persistenceEngine.Container = _dataContainer;
                            _dataContainer.PersistenceEngine = _persistenceEngine;

                            // reinitialize data container                          
                            _dataContainer.LoadSchema(Path.Combine(dataPath, Constants.SchemaFileName));


                            // schema needs to be updated because the dump contains a single instance (the one for shard 0) so the 
                            // ShardIndex may be incorrect
                            _dataContainer.ShardIndex = importRequest.ShardIndex;
                            var schema = _dataContainer.GenerateSchema();
                            _persistenceEngine?.UpdateSchema(schema);

                            // fill the in-memory data stores
                            Parallel.ForEach(_dataContainer.DataStores.Values,
                                store => store.LoadFromDump(path, importRequest.ShardIndex));

                            // write to the persistent storage (this is the only case where we write directly in the storage, not in the transaction log)
                            foreach (var dataStore in _dataContainer.DataStores.Values)
                            {
                                foreach (var item in dataStore.DataByPrimaryKey)
                                {
                                    var itemData =
                                        SerializationHelper.ObjectToBytes(item.Value, SerializationMode.ProtocolBuffers,
                                            dataStore.TypeDescription);

                                    storage.StoreBlock(itemData, item.Key.ToString(), 0);
                                }
                            }

                            // import the sequences

                            var sequenceFile = $"sequence_{importRequest.ShardIndex:D3}.json";

                            _dataContainer.LoadSequence(Path.Combine(path, sequenceFile));
                            File.Copy(Path.Combine(path, sequenceFile),
                                Path.Combine(dataPath, Constants.SequenceFileName));
                        }

                        break;
                    case 2: // all good
                        _persistenceEngine.LightStart();

                        foreach (var dataStore in _dataContainer.DataStores.Values)
                        {
                            dataStore.StartProcessingClientRequests();
                        }

                        _persistenceEngine.DeleteRollbackData();
                        Mode = ServerMode.Normal;
                        break;

                    case 3: // something bad happened. Rollback
                        _persistenceEngine.RollbackData();

                        _dataContainer = new DataContainer(new ServerConfig(), _serverProfiler);
                        _persistenceEngine.Container = _dataContainer;
                        _dataContainer.PersistenceEngine = _persistenceEngine;

                        _persistenceEngine.Start();

                        Mode = ServerMode.Normal;

                        foreach (var dataStore in _dataContainer.DataStores.Values)
                        {
                            dataStore.StartProcessingClientRequests();
                        }

                        break;

                    default:
                        client.SendResponse(new ExceptionResponse(
                            new NotSupportedException(
                                $"Illegal value {importRequest.Stage} for parameter stage in import request")));
                        return;
                }

                ServerLog.LogInfo(
                    $"end successfull import dump stage {importRequest.Stage} on shard {importRequest.ShardIndex}");
                client.SendResponse(new NullResponse());
            }
            catch (AggregateException e)
            {
                ServerLog.LogError(
                    $"end  import dump stage {importRequest.Stage} on shard {importRequest.ShardIndex} with exception {e.Message}");
                client.SendResponse(new ExceptionResponse(e.InnerException));
            }
            catch (Exception e)
            {
                client.SendResponse(new ExceptionResponse(e));
            }
        }

        

        public void Start()
        {
            Dbg.Trace("starting server");

            _serverProfiler.IsActive = true;
            _startTime = DateTime.Now;
            

            if (IsPersistent)
            {
                Dbg.Trace("starting persistence engine");
                _persistenceEngine = new PersistenceEngine(_dataContainer, _workingDirectory);
                _dataContainer.PersistenceEngine = _persistenceEngine;

                _persistenceEngine.Start();
            }

            // start processing client requests
            foreach (var dataStore in _dataContainer.DataStores.Values)
            {
                dataStore.StartProcessingClientRequests();
            }
        }

        public void Stop()
        {
            
            foreach (KeyValuePair<string, DataStore> keyValuePair in _dataContainer.DataStores)
            {
                keyValuePair.Value.Stop();
            }

            _persistenceEngine?.Stop();

            Dbg.Trace("SERVER STOPPED");
        }

        public void WaitForServerInitialization()
        {
            _eventAllLoadersFinished.WaitOne();
        }
    }
}