﻿using System.Collections.Generic;
using CacheClient.Core;
using ProtoBuf;

namespace Cachealot.Server.Persistence
{

    [ProtoContract]
    [ProtoInclude(500, typeof(PutTransaction))]
    [ProtoInclude(600, typeof(DeleteTransaction))]
    [ProtoInclude(700, typeof(MixedTransaction))]
    public abstract class Transaction
    {
        /// <summary>
        /// Used by protobuf serialization
        /// </summary>
        // ReSharper disable once EmptyConstructor
        // ReSharper disable once PublicConstructorInAbstractClass
        public Transaction()
        {
        }
    }


    /// <summary>
    /// Transaction containing multiple items to update or insert
    /// </summary>
    [ProtoContract]
    public class PutTransaction : Transaction
    {
        [ProtoMember(10)]
        public IList<CachedObject> Items { get; set; } = new List<CachedObject>();

        /// <summary>
        /// Used by protobuf serialization
        /// </summary>
        // ReSharper disable once EmptyConstructor
        // ReSharper disable once PublicConstructorInAbstractClass
        public PutTransaction()
        {
        }
    }


    /// <summary>
    /// Transaction containing the primary keys of the items to delete
    /// </summary>
    [ProtoContract]
    public class DeleteTransaction : Transaction
    {
        [ProtoMember(20)]
        public IList<KeyValue> PrimaryKeys { get; set; } = new List<KeyValue>();

        /// <summary>
        /// Used by protobuf serialization
        /// </summary>
        // ReSharper disable once EmptyConstructor
        // ReSharper disable once PublicConstructorInAbstractClass
        public DeleteTransaction()
        {
        }
    }


    /// <summary>
    /// A transaction containing both Put and delete requests
    /// </summary>
    [ProtoContract]
    public class MixedTransaction : Transaction
    {

        [ProtoMember(10)]
        public IList<CachedObject> ItemsToPut { get; set; } = new List<CachedObject>();


        [ProtoMember(20)]
        public IList<KeyValue> ItemsToDelete { get; set; } = new List<KeyValue>();

        /// <summary>
        /// Used by protobuf serialization
        /// </summary>
        // ReSharper disable once EmptyConstructor
        // ReSharper disable once PublicConstructorInAbstractClass
        public MixedTransaction()
        {
        }
    }
}
