﻿namespace Cachealot.Server.Persistence
{
    public enum BlockStatus
    {
        Active,
        Deleted,
        Dirty
    }
}