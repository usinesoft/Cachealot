namespace Cachealot.Server.Persistence
{
    public enum TransactionStaus
    {
        ToProcess,
        Processing,
        Processed,
        Canceled
    }
}