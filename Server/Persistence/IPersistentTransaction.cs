using System;

namespace Cachealot.Server.Persistence
{
    public interface IPersistentTransaction
    {
        byte[] Data { get; }
        DateTime TimeStamp { get; }
        TransactionStaus TransactionStaus { get; }

        long Id { get; }
    }
}