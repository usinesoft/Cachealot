﻿namespace Cachealot.Server.Persistence
{
    public static class Constants
    {
        public static readonly string DataPath = "data";
        public static readonly string RollbackDataPath = "_data";
        public static readonly string SchemaFileName = "schema.json";
        public static readonly string SequenceFileName = "sequence.json";
        public static readonly string NodeConfigFileName = "node_config.json";

        public static readonly int DefaultPort = 4848;

        public static readonly int BulkThreshold = 50;
        
    }
}
